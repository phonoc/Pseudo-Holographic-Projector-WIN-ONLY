#include "image.hpp"
#include "logger.hpp"

Image::Image(const std::string & fpath) {
	_imageSurface = nullptr;
	_imageSurface = IMG_Load(fpath.c_str());
	if (_imageSurface == nullptr) {
		logger::putError("Unable to load image file: " + std::string(IMG_GetError()));
		std::exit(EXIT_FAILURE);
	}
}

Image::~Image() {
	if (_imageSurface != nullptr) {
		SDL_FreeSurface(_imageSurface);
	}
}

SDL_Surface * Image::getSurface() {
	return _imageSurface;
}

