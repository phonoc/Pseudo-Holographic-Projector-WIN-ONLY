#ifndef SHAPE_H
#define SHAPE_H

#include <vector>

#include "glm\glm.hpp"

#include "image.hpp"
#include "shader.hpp"
#include "camera.hpp"

typedef glm::vec3 Coordinate;

typedef struct {
	GLfloat r, g, b, a;
} Color;

namespace Mesh {

	class Mesh {
		private:
			std::string _name;
			float _rotate = 0.0f;
			float _translate = 0.0f;

		protected:
			Coordinate _position;
			GLfloat *_vertices;
			GLuint *_vertexBufferObjects;
			GLuint *_vertexArrayObjects;
			GLuint *_textures;
			size_t _vertices_size;
			Shader *_shader;
			Camera *_camera = nullptr;

			glm::mat4 _modelMat, _viewMat, _projectionMat;
			glm::mat4 _mvpMat;
			glm::mat4 _lightSpaceMat;
			glm::vec3 _normal;

			void updateMVP();
			void useShader() const;
			GLuint getProgram() const;

			virtual void onPreRender() = 0;
			virtual void onRender() = 0;

		public:
			Mesh();
			~Mesh();

			virtual void bindCamera(Camera *cam);
			virtual void unBindCamera();
			virtual void bindShader(Shader *shader);
			virtual void applyTexture(SDL_Surface *surface);
			virtual void applyTexture(void * pixels, const int w, const int h);
			virtual void translateTo(const Coordinate & dest);
			virtual void translateBy(const float dist, const glm::vec3& dir);
			virtual void rotateTo(const float deg, const glm::vec3& axis);
			virtual void rotateBy(const float deg, const glm::vec3& axis);

			virtual void render();
			virtual void renderSkip();

			void setName(const std::string& newName);
			virtual void setLightSpaceMat(const glm::mat4 & mat);

			virtual Coordinate getPosition();
			glm::vec3 getNormalVector();
			Shader *getShader();
			std::string getName();
	};

	class LightSource : public Mesh {
		private:
			Color _color;

		public:
			LightSource(const Coordinate& pos, const Color& color);
			~LightSource();

			void applyTexture(SDL_Surface *surface);
			void applyTexture(void * pixels, const int w, const int h);
			void setColor(const Color& newColor);

			Color getColor();

			void onPreRender();
			void onRender();
	};

	class Triangle : public Mesh {
		public:
			Triangle(const Coordinate& p1, const Coordinate& p2, const Coordinate& p3);
			~Triangle();

			void onPreRender();
			void onRender();
	};

	class Rectangle : public Mesh {
		private:
			GLuint _elementBufferObject;
			GLuint *_drawIndices;
			LightSource * _lightSource = nullptr;

		public:

			Rectangle(const Coordinate& p1,
				const Coordinate& p2,
				const Coordinate& p3,
				const Coordinate& p4,
				const glm::vec3& normal
			);

			Rectangle(const GLfloat width, const GLfloat height, const glm::vec3& normal);
			~Rectangle();

			void onPreRender();
			void onRender();
			void bindLightSource(LightSource * src);
			void unbindLightSource();
	};

	class Building : public Mesh {
		private:
			Rectangle *_walls[4];
			Rectangle *_roof;
			Coordinate _abstractPosition;

		public:
			Building(const Coordinate& upperLeft, const GLfloat width, const GLfloat height);
			~Building();

			void bindCamera(Camera *cam);
			void unBindCamera();
			void bindShader(Shader *shader);
			void applyTexture(SDL_Surface *surface);
			void applyTexture(void * pixels, const int w, const int h);
			virtual void translateTo(const Coordinate& dest);
			void translateBy(const float dist, const glm::vec3& axis);
			void rotateTo(const float deg, const glm::vec3& axis);
			void rotateBy(const float deg, const glm::vec3& axis);
			
			void onPreRender();
			void onRender();
			void renderSkip();
			void bindLightSource(LightSource * src);
			void unbindLightSource();

			void setLightSpaceMat(const glm::mat4 & mat);

			Coordinate getPosition();
	};

} /* namespace Mesh */

#endif /* SHAPE_H */
