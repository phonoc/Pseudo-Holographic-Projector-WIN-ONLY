#ifndef MODEL_H
#define MODEL_H

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "mesh.hpp"


typedef struct {
	glm::vec3 position; // xyz
	glm::vec3 normal;
	glm::vec2 texCoords; // uv
	glm::vec3 tangent;
	glm::vec3 bitangent;
} Vertex;

typedef struct {
	GLuint id;
	std::string type;
	aiString path;
} Texture;

class MeshModel {
	private:
		Shader * _shader;
		Camera * _camera;
		Mesh::LightSource *_lightSource;

		GLuint _vertexArrayObject, _vertexBufferObject, _elementBufferObject;
		void onRender(const glm::mat4 & mvp, const glm::mat4 & model);
		void onPreRender();
		GLuint getProgram();

	public:
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture> textures;

		MeshModel(const std::vector<Vertex>& vv, const std::vector<GLuint>& iv, const std::vector<Texture>& tv);
		~MeshModel();

		void bindCamera(Camera *cam);
		void unBindCamera();
		void bindShader(Shader *shader);
		void bindLightSource(Mesh::LightSource *src);

		void setup();
		void clean();
		void render(const glm::mat4 & mvp, const glm::mat4 model);
		void renderSkip(const glm::mat4 & mvp, const glm::mat4 model);
};

class Model {
	private:
		std::vector<MeshModel> _meshes;
		Coordinate _abstractPosition;
		Shader * _shader;
		Camera * _camera;
		Mesh::LightSource *_lightSource;

		glm::mat4 _modelMat, _viewMat, _projectionMat;
		glm::mat4 _mvpMat;

		std::string _name;
		std::string _directory;
		float _rotate = 0.0f;
		float _translate = 0.0f;

		char * _asmPixels;
		int _asmWidth, _asmHeight;
		GLuint _loadedAsmTexture;
		bool _asmTextureReady = false;
		uint32_t _asmSize;
		std::vector<Texture> _loadedTextures;

		void updateMVP();
		void useShader() const;

		// Private methods
		GLuint loadTextureFromPixels(void * pixels, const int w, const int h);
		GLuint loadTextureFromFile(const char *fname, const std::string & dir);
		GLuint loadTextureFromFileRW(char *pngData, const uint32_t dataSize);
		std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
		MeshModel processMesh(aiMesh* mesh, const aiScene* scene);

		void loadModel(const std::string& path);
		void processNode(aiNode* node, const aiScene* scene);

	public:
		Model(const std::string & path_to_obj);
		Model(const std::string & path_to_obj, void * asm_pixels, const int w, const int h);
		Model(const std::string & path_to_obj, void * asm_png_data, const uint32_t data_size);

		void bindCamera(Camera *cam);
		void unBindCamera();
		void bindShader(Shader *shader);
		void bindLightSource(Mesh::LightSource *src);

		void translateTo(const Coordinate & dest);
		void translateBy(const float dist, const glm::vec3& dir);
		void rotateTo(const float deg, const glm::vec3& axis);
		void rotateBy(const float deg, const glm::vec3& axis);

		Shader * getShader();

		void clean();
		void render();
		void renderSkip();
};

#endif /* MODEL_H */
