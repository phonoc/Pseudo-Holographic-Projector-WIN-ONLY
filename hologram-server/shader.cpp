#include "shader.hpp"
#include "logger.hpp"

Shader::Shader() {}

Shader::Shader(const std::string& vs_path, const std::string& fs_path) {
	_shaderProgramID = loadShaderProgram(vs_path, fs_path);
}

Shader::~Shader() {
	glDeleteShader(_vertexShader);
	glDeleteShader(_fragmentShader);
	glDeleteProgram(_shaderProgramID);
}

void Shader::use() {
	glUseProgram(_shaderProgramID);
}

GLuint Shader::getProgram() const {
	return _shaderProgramID;
}

GLuint Shader::loadShaderProgram(const std::string& vs_path, const std::string& fs_path){

    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER),
           fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    
    /* Read shader source code */
	FILE *vs_stream = nullptr;
	errno_t e = fopen_s(&vs_stream, vs_path.c_str(), "r");
    if (vs_stream == nullptr){
		logger::putError("Unable to open vertex shader file: code " + std::to_string(e));
        std::exit(EXIT_FAILURE);
    }
	FILE *fs_stream = nullptr;
	e = fopen_s(&fs_stream, fs_path.c_str(), "r");
    if (fs_stream == nullptr){
		logger::putError("Unable to open fragment shader file: code " + std::to_string(e));
        std::exit(EXIT_FAILURE);
    }

    std::string vsCode;
    char byte_read;
    while ((byte_read = fgetc(vs_stream)) != EOF){
        vsCode.push_back(byte_read);
    }
    vsCode.push_back(0); /* terminate with NUL */
    fclose(vs_stream);

    std::string fsCode;
    while ((byte_read = fgetc(fs_stream)) != EOF){
        fsCode.push_back(byte_read);
    }
    fsCode.push_back(0);
    fclose(fs_stream);

    /* Compile vertex shader */
	logger::putInfo("Compiling vertex shader '" + vs_path + "'...");
    fflush(stdout);
    const char *vsCode_c = vsCode.c_str();
    glShaderSource(vertexShaderID, 1, &vsCode_c, nullptr);
    glCompileShader(vertexShaderID);

    /* Check validity of the compiled vertex shader */
    GLint compilationResult = 0;
    GLint infoLogLength = 0;
    glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &compilationResult);
    glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (!compilationResult){

        char *compilationLog = new char[infoLogLength + 1];
        glGetShaderInfoLog(vertexShaderID, infoLogLength, nullptr, compilationLog);
        compilationLog[infoLogLength] = 0;
		logger::putError("Compilation error: " + std::string(compilationLog));
		delete compilationLog;
        std::exit(EXIT_FAILURE);
    }

    /* Compile fragment shader */
    logger::putInfo("Compiling fragment shader '" + fs_path + "'...");
    fflush(stdout);
    const char *fsCode_c = fsCode.c_str();
    glShaderSource(fragmentShaderID, 1, &fsCode_c, nullptr);
    glCompileShader(fragmentShaderID);

    /* Check validity of the compiled fragment shader */
    compilationResult = 0;
    infoLogLength = 0;
    glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &compilationResult);
    glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (!compilationResult){

        char *compilationLog = new char[infoLogLength + 1];
        glGetShaderInfoLog(fragmentShaderID, infoLogLength, nullptr, compilationLog);
        compilationLog[infoLogLength] = 0;
		logger::putError("Compilation error: " + std::string(compilationLog));
		delete compilationLog;
        std::exit(EXIT_FAILURE);
    }

    /* Link shader program */
	logger::putInfo("Linking shader program...");
    fflush(stdout);
    const GLuint programID = glCreateProgram();
    glAttachShader(programID, vertexShaderID);
    glAttachShader(programID, fragmentShaderID);
    glLinkProgram(programID);

    GLint linkingResult = 0;
    infoLogLength = 0;
    glGetProgramiv(programID, GL_LINK_STATUS, &linkingResult);
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (!linkingResult){

        char *linkingLog = new char[infoLogLength + 1];
        glGetProgramInfoLog(programID, infoLogLength, nullptr, linkingLog);
        linkingLog[infoLogLength] = 0;
		logger::putError("Linking error: " + std::string(linkingLog));
		delete linkingLog;
        std::exit(EXIT_FAILURE);
    }
	
	_vertexShader = vertexShaderID;
	_fragmentShader = fragmentShaderID;
	logger::putInfo("Shader program loaded successfully.");
    fflush(stdout);
    return programID;
}
