#ifndef SWITCH_H
#define SWITCH_H

#include "logger.hpp"

class Switch {

	public:
		/* Methods */
		bool parse(int argc, char **argv);
};

#endif /* SWITCH_H */
