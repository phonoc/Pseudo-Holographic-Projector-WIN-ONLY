#pragma comment (lib, "Ws2_32.lib")	/* Use Windows socket version 2 */

/* Standard Libraries */
#include <cstdint>
#include <cmath>

/* Additional Libraries */
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "SDL.h"
#include "GL\glew.h"

/* Local Dependencies */
#include "logger.hpp"
#include "switch.hpp"
#include "hard_config.hpp"
#include "config.hpp"
#include "map.hpp"
#include "connection.hpp"
#include "model.hpp"
#include "shadowmap.hpp"
#include "image.hpp"
#include "building_types.hpp"

void initLogger() {
	logger::setMonOut(true);
	logger::start("hologram-server.log");
	std::atexit(logger::stop);
}

void initWinsock() {
	WSADATA wsaData;
	int initResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (initResult) {
		logger::putError("Failed to initialize WinSock");
		std::exit(EXIT_FAILURE);
	}
}

void initGLAttributes() {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16); // 16xMSAA
}

void initGL() {
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glViewport(0, 0, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT());
}

void initSDL() {
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	SDL_DisableScreenSaver(); // very important!
	SDL_ShowCursor(SDL_DISABLE);
}

void cleanUp() {
	WSACleanup();
	IMG_Quit();
	SDL_Quit();
}

class MainLoop {

	private:
		SDL_Window * mainWindow;
		const int msPerFrame = 1000 / CONFIG::GET_FPS();
		bool isRunning = true;
		Connection *imageReceiver;
		Shader *modelShader, *shaderProgram, *lightShaderProgram;
		Image *imageWall, *imagePlane, *imageSign;

		Camera *camera, *mockCamera;
		Mesh::LightSource *lightSource;
		Model * building = nullptr;
		Mesh::Rectangle *plane, *sign;
		ShadowMap *shadowMap;

		uint32_t clock = SDL_GetTicks();
		uint32_t clock_firstFrame = clock;
		uint32_t acc_difftime = 0;
		uint32_t frame_count = 0;

		bool showLamp = false;
		double diffTime;
		const uint8_t *keyState;
		char *imageData = nullptr;

		void fetchEvents() {
			/* Fetch events */
			SDL_Event event;
			while (SDL_PollEvent(&event)) {

				/* Handle events */
				switch (event.type) {
				case SDL_WINDOWEVENT_CLOSE:
					/* User terminate the program (e.g. by clicking X) */
					isRunning = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
					case SDLK_SPACE:
						break;
					case SDLK_ESCAPE:
						imageReceiver->stopListening();
						isRunning = false;
						break;
					case SDLK_l:
						showLamp = !showLamp;
						break;
					}
					break;
				case SDL_MOUSEWHEEL:
					if (event.wheel.y < 0) {
						camera->moveBackward(-1.0f * event.wheel.y);
					}
					else {
						camera->moveForward(1.0f * event.wheel.y);
					}
					break;
				default:
					/* Unknown event type; ignore */
					break;
				}
			}
		}

		void renderObjects() {
			/* Clear back buffer */
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			/* Render shadow map */
			shadowMap->render();

			/* Draw objects that aren't bound to shadow map */
			if (showLamp) lightSource->render();
			plane->render();
			if (building == nullptr) {
				sign->render();
			}
		}

		void renderBuilding() {
			if (building != nullptr) building->render();
		}

		void checkData() {
			/* Check for incoming data */
			if (imageReceiver->isDataReady()) {
				if (imageReceiver->isDataChanged()) {
					uint32_t type = imageReceiver->getType();
					logger::putInfo("New texture arrived. (type " + std::to_string(type) + ")");
					imageData = imageReceiver->getData();
					if (type >= BUILDING0 && type <= BUILDING2) {

						shadowMap->unbindModel(building);

						// show "Under Construction" sign
						renderObjects();
						sign->render();
						SDL_GL_SwapWindow(mainWindow);
						SDL_Delay(100);

						if (building != nullptr) {
							building->clean(); // clear buffers on VRAM
							delete building;
							building = nullptr;
						}
						std::string path = std::to_string(type - 1);

						// create building
						building = new Model("0" + path + "/" + "b0" + path + ".obj", imageData, imageReceiver->getSize());
						building->bindCamera(camera);
						building->bindLightSource(lightSource);
						building->bindShader(modelShader);
						shadowMap->bindModel(building);

						// hide "Under Construction" sign
						renderObjects();
					}
					else {
						if (type) { // in case type is termination opcode
							logger::putError("**CRITICAL** Unknown building type: " + std::to_string(type));
						}
					}
				}
			}
		}

		void animateObjects() {
			/* Animate objects */
			float cameraStep = 0.005f * diffTime, cameraRotateStep = 0.05f * diffTime;
			float rotateStep = 0.01 * diffTime;
			if (cameraStep == 0.000000f) cameraStep = 0.005f;
			if (cameraRotateStep == 0.000000f) cameraRotateStep = 0.1f;

			if (building != nullptr) building->rotateBy(rotateStep, glm::vec3(0.0f, 1.0f, 0.0f));
			plane->rotateBy(rotateStep, glm::vec3(0.0f, 1.0f, 0.0f));

			/* Handle keystroke */
			if (keyState[SDL_SCANCODE_W] || keyState[SDL_SCANCODE_UP])
				if (keyState[SDL_SCANCODE_LSHIFT])
					camera->rotateUp(cameraRotateStep);
				else
					camera->moveUp(cameraStep);

			if (keyState[SDL_SCANCODE_S] || keyState[SDL_SCANCODE_DOWN])
				if (keyState[SDL_SCANCODE_LSHIFT])
					camera->rotateDown(cameraRotateStep);
				else
					camera->moveDown(cameraStep);

			if (keyState[SDL_SCANCODE_A] || keyState[SDL_SCANCODE_LEFT])
				if (keyState[SDL_SCANCODE_LSHIFT])
					camera->rotateYBy(-cameraRotateStep);
				else
					camera->moveLeft(cameraStep);

			if (keyState[SDL_SCANCODE_D] || keyState[SDL_SCANCODE_RIGHT])
				if (keyState[SDL_SCANCODE_LSHIFT])
					camera->rotateYBy(cameraRotateStep);
				else
					camera->moveRight(cameraStep);
		}

	public:
		MainLoop(SDL_Window *window) {
			mainWindow = window;
			imageReceiver = new Connection(CONFIG::GET_PORT());

			// shaders
			modelShader = new Shader("shaders/modelVertex.glsl", "shaders/modelFragment.glsl");
			shaderProgram = new Shader("shaders/vertex.glsl", "shaders/fragment.glsl");
			lightShaderProgram = new Shader("shaders/lightVertex.glsl", "shaders/lightFragment.glsl");

			// images
			imageWall = new Image("windows.jpg");
			imagePlane = new Image("road.png");
			imageSign = new Image("construction_sign.png");

			// objects

			/* Cameras */
			camera = new Camera(glm::vec3(0.0f, 10.0f, 20.0f), glm::vec3(0.0f, 0.0f, 0.0f));
			mockCamera = new Camera(glm::vec3(0.0f, 0.0f, 20.0f), glm::vec3(0.0f, 0.0f, 0.0f));

			/* Light source cube */
			lightSource = new Mesh::LightSource({ 2.0f, 3.0f, 2.0f }, { 1.0f, 1.0f, 1.0f, 1.0f });
			lightSource->bindCamera(camera);
			lightSource->bindShader(lightShaderProgram);
			lightSource->setName("Light");

			/* Building */
			Model * building = nullptr;

			/* Rectangle plane */
			plane = new Mesh::Rectangle(CONFIG::GET_PLANE_WIDTH(), CONFIG::GET_PLANE_WIDTH(), glm::vec3(0.0f, 1.0f, 0.0f));
			plane->bindCamera(camera);
			plane->bindShader(shaderProgram);
			plane->applyTexture(imagePlane->getSurface());
			plane->bindLightSource(lightSource);
			plane->setName("Plane");

			/* Construction sign */
			sign = new Mesh::Rectangle(
				{ -5.0f, 0.0f, 0.0f },
				{ -5.0f, 10.0f, 0.0f },
				{ 5.0f, 10.0f, 0.0f },
				{ 5.0f, 0.0f, 0.0f },
				{ 0.0f, 0.0f, 1.0f }
			);
			sign->applyTexture(imageSign->getSurface());
			sign->bindLightSource(lightSource);
			sign->setName("Sign");
			sign->bindShader(shaderProgram);
			sign->bindCamera(mockCamera);

			/* Shadow map */
			shadowMap = new ShadowMap(lightSource);
			shadowMap->bindMesh(plane);
			shadowMap->bindMesh(sign);
		}

		~MainLoop() {
			delete imageReceiver;
			delete modelShader, shaderProgram, lightShaderProgram;
			delete imageWall, imagePlane, imageSign;
			delete camera, mockCamera, lightSource, plane, sign, shadowMap;
			if (building != nullptr) {
				building->clean();
				delete building;
			}
		}

		void run() {
			while (isRunning) {
				diffTime = SDL_GetTicks() - clock;
				clock = SDL_GetTicks();

				if (!imageReceiver->isListening()) imageReceiver->startListening();

				keyState = SDL_GetKeyboardState(nullptr);
				fetchEvents();
				checkData();
				animateObjects();
				renderObjects();
				renderBuilding();
				SDL_GL_SwapWindow(mainWindow);

				/* Show FPS */
				acc_difftime += diffTime;
				++frame_count;

				if (std::ceil(acc_difftime / 1000) >= 5) {
					logger::putInfo("FPS: " + std::to_string(1.0f / ((double)acc_difftime / frame_count / 1000.0f)));
					acc_difftime = 0;
					frame_count = 0;
				}

				/* Cap frame rate if configured to do so */
				if (CONFIG::GET_IS_FPS_CAP()) {
					if (diffTime < msPerFrame) {
						SDL_Delay(msPerFrame - diffTime);
					}
				}
			}
		}

};

int main(int argc, char** argv) {

	initLogger();
	initWinsock();
	std::atexit(cleanUp);

	Switch options;
	if (!options.parse(argc, argv)) return EXIT_FAILURE;

	if (!SDL_Init(SDL_INIT_VIDEO)) {

		initSDL();
		initGLAttributes();

		uint32_t windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		if (!CONFIG::GET_IS_WINDOWED()) {
			windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		/* Create main window */
		SDL_Window *mainWindow = SDL_CreateWindow(
			HARDCONFIG_APP_NAME,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			CONFIG::GET_WIN_WIDTH(),
			CONFIG::GET_WIN_HEIGHT(),
			windowFlags
		);

		if (mainWindow == nullptr) {
			logger::putError("Unable to create window: " + std::string(SDL_GetError()));
			std::exit(EXIT_FAILURE);
		}

		/* Initialize SDL_image */
		if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) != (IMG_INIT_JPG | IMG_INIT_PNG)) {
			logger::putError("Unable to initialize SDL_image: " + std::string(IMG_GetError()));
			std::exit(EXIT_FAILURE);
		}

		/* Initialize OpenGL */
		SDL_GLContext mainContext = SDL_GL_CreateContext(mainWindow);
		if (mainContext == nullptr) {
			logger::putError("Unable to bind OpenGL context: " + std::string(SDL_GetError()));
			std::exit(EXIT_FAILURE);
		}
		glewExperimental = GL_TRUE;
		glewInit();

		initGL();

		logger::putInfo("OpenGL version: " + std::string((char *) glGetString(GL_VERSION)));
		logger::putInfo("GLSL version: " + std::string((char *) glGetString(GL_SHADING_LANGUAGE_VERSION)));
		logger::putInfo("Hardware vendor: " + std::string((char *)glGetString(GL_VENDOR)));

		/* Everything seems fine. Enter main loop. */
		MainLoop mainLoop(mainWindow);
		mainLoop.run();

		/* Clean up */
		SDL_GL_DeleteContext(mainContext);
		SDL_DestroyWindow(mainWindow);
	}
	else {
		return logger::putError("Failed to initialize SDL: " + std::string(SDL_GetError()));
	}

	return EXIT_SUCCESS;
}
