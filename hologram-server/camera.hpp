#ifndef CAMERA_H
#define CAMERA_H

#include "glm\glm.hpp"

class Camera {

	private:
		glm::vec3 _position, _target, _direction;
		glm::mat4 _lookAt;

		void updateValues();

	public:
		Camera(const glm::vec3& pos, const glm::vec3& target);

		void moveTo(const glm::vec3& pos);
		void moveBy(const glm::vec3& dist);
		void rotateBy(const float deg, const glm::vec3& axis);
		void rotateYBy(const float deg);

		void rotateUp(const float deg);
		void rotateDown(const float deg);
		void moveForward(const float dist);
		void moveBackward(const float dist);
		void moveLeft(const float dist);
		void moveRight(const float dist);
		void moveUp(const float dist);
		void moveDown(const float dist);

		glm::mat4 lookAt(const glm::vec3& target);
		glm::mat4 getTranslationMat();

		glm::vec3 getPosition();
};

#endif /* CAMERA_H */
