#include "mesh.hpp"
#include "config.hpp"
#include "logger.hpp"

#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_transform.hpp"

namespace Mesh {

	Mesh::Mesh() {
		_position = { 0.0f, 0.0f, 0.0f };
		_shader = nullptr;
		_vertices = nullptr;
		_textures = nullptr;

		_viewMat = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));
		_modelMat = glm::rotate(glm::mat4(), 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		_projectionMat = glm::perspective(
			45.0f,
			(float) CONFIG::GET_WIN_WIDTH() / (float) CONFIG::GET_WIN_HEIGHT(),
			0.1f,
			100.0f
		);

		updateMVP();
	}

	Mesh::~Mesh() {
		if (_vertices != nullptr) {
			delete _vertices;
		}
	}

	void Mesh::bindCamera(Camera * cam) {
		_camera = cam;
	}

	void Mesh::unBindCamera() {
		_camera = nullptr;
	}

	void Mesh::bindShader(Shader * shader) {
		_shader = shader;
	}

	void Mesh::applyTexture(SDL_Surface * surface) {
		if (_textures == nullptr) return;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindTexture(GL_TEXTURE_2D, *_textures);
		glTexImage2D(
			GL_TEXTURE_2D,
			NULL,
			GL_RGBA,
			surface->w,
			surface->h,
			NULL,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			surface->pixels
		);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, NULL);
	}

	void Mesh::applyTexture(void * pixels, const int w, const int h) {
		if (_textures == nullptr) return;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindTexture(GL_TEXTURE_2D, *_textures);
		glTexImage2D(
			GL_TEXTURE_2D,
			NULL,
			GL_RGBA,
			w,
			h,
			NULL,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			pixels
		);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, NULL);
	}

	void Mesh::translateTo(const Coordinate& dest) {
		glm::mat4 transMat = glm::translate(glm::mat4(), dest);
		_viewMat = transMat;
		updateMVP();
	}

	void Mesh::translateBy(const float dist, const glm::vec3 & axis) {
		glm::mat4 transMat = glm::translate(glm::mat4(), dist * axis);
		_viewMat = _viewMat * transMat;
		updateMVP();
	}

	void Mesh::rotateTo(const float deg, const glm::vec3& axis) {
		_rotate = deg;
		_modelMat = glm::rotate(glm::mat4(), glm::radians(_rotate), axis);
		updateMVP();
	}

	void Mesh::rotateBy(float deg, const glm::vec3& axis) {
		_rotate += deg;
		if (_rotate < -360.0f) {
			_rotate += -360.0f;
		}
		else if (_rotate > 360.0f) {
			_rotate -= 360.0f;
		}
		rotateTo(_rotate, axis);
	}

	void Mesh::render() {
		this->useShader();
		updateMVP();
		onPreRender();
		onRender();
	}

	void Mesh::renderSkip() {
		this->useShader();
		updateMVP();
		onRender();
	}

	void Mesh::setName(const std::string & newName) {
		_name = newName;
	}

	void Mesh::setLightSpaceMat(const glm::mat4 & mat) {
		_lightSpaceMat = mat;
	}

	Coordinate Mesh::getPosition() {
		return glm::vec3(_viewMat * _modelMat * glm::vec4(_position, 1.0f)) - _position;
	}

	glm::vec3 Mesh::getNormalVector() {
		return glm::transpose(glm::inverse(glm::mat3(_modelMat))) * _normal;
	}

	Shader * Mesh::getShader() {
		return _shader;
	}

	std::string Mesh::getName() {
		return _name;
	}

	void Mesh::updateMVP() {
		if (_camera != nullptr) {
			_mvpMat = _projectionMat * (_camera->getTranslationMat() * _viewMat) * _modelMat;
		}
		else {
			_mvpMat = _projectionMat * _viewMat * _modelMat;
		}
	}

	void Mesh::useShader() const {
		if (_shader != nullptr) {
			_shader->use();
		}
		else {
			logger::putWarning(_name + ": Using null shader, this may lead into problems!");
		}
	}

	GLuint Mesh::getProgram() const {
		if (_shader != nullptr)
			return _shader->getProgram();
		return NULL;
	}

	Triangle::Triangle(const Coordinate & p1, const Coordinate & p2, const Coordinate & p3) {

		_vertexBufferObjects = new GLuint[1];
		_vertexArrayObjects = new GLuint[1];
		_textures = new GLuint[1];
		glGenBuffers(1, _vertexBufferObjects);

		_vertices_size = 24;
		_vertices = new GLfloat[_vertices_size]
		{
			// Coordinates		// Colors			// Texture Coordinates
			p1.x, p1.y, p1.z,	1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
			p2.x, p2.y, p2.z,	0.0f, 1.0f, 0.0f,	0.5f, 1.0f,
			p3.x, p3.y, p3.z,	0.0f, 0.0f, 1.0f,	1.0f, 0.0f
		};
		glGenVertexArrays(1, _vertexArrayObjects);
		glGenTextures(1, _textures);

		glBindVertexArray(*_vertexArrayObjects);
			glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects);
			glBufferData(GL_ARRAY_BUFFER, _vertices_size * sizeof(GLfloat), _vertices, GL_STATIC_DRAW);
		glBindVertexArray(NULL);
	}

	Triangle::~Triangle() {
		glDeleteBuffers(1, _vertexBufferObjects);
		glDeleteVertexArrays(1, _vertexArrayObjects);
		glDeleteTextures(1, _textures);
	}

	void Triangle::onPreRender() { /* TODO */ }

	void Triangle::onRender() {
		const GLuint positionIndex = 0;
		const GLuint colorIndex = 1;
		const GLuint texIndex = 2;
		const GLuint uniMVPIndex = glGetUniformLocation(getProgram(), "mvp");
		const GLuint uniModelIndex = glGetUniformLocation(getProgram(), "model");
		const GLuint uniLightColorIndex = glGetUniformLocation(getProgram(), "lightColor");
		const GLsizei stride = 8 * sizeof(GLfloat);

		useShader();
		glBindVertexArray(*_vertexArrayObjects);

			glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects); // very important!

			/* Uniforms */
			glUniformMatrix4fv(uniMVPIndex, 1, GL_FALSE, glm::value_ptr(_mvpMat));
			glUniform3f(uniLightColorIndex, 1.0f, 1.0f, 1.0f);
			glUniformMatrix4fv(uniModelIndex, 1, GL_FALSE, glm::value_ptr(_modelMat));

			/* position */
			glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, stride, nullptr);
			glEnableVertexAttribArray(positionIndex);

			/* newColor_in */
			glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(colorIndex);

			/* texCoord_in */
			glVertexAttribPointer(texIndex, 2, GL_FLOAT, GL_FALSE, stride, (void*)(6 * sizeof(GLfloat)));
			glEnableVertexAttribArray(texIndex);

			/* Draw */
			glBindTexture(GL_TEXTURE_2D, *_textures);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			/* Clean up */
			glBindTexture(GL_TEXTURE_2D, NULL);
			glDisableVertexAttribArray(positionIndex);
			glDisableVertexAttribArray(colorIndex);
			glDisableVertexAttribArray(texIndex);

		glBindVertexArray(NULL);
	}

	Rectangle::Rectangle(const Coordinate & p1, const Coordinate & p2, const Coordinate & p3, const Coordinate & p4, const glm::vec3& normal) {

		_normal = normal;
		_vertexBufferObjects = new GLuint[1];
		_vertexArrayObjects = new GLuint[1];
		_textures = new GLuint[1];
		glGenBuffers(1, _vertexBufferObjects);

		_vertices_size = 32;
		_vertices = new GLfloat[_vertices_size]
		{
			// Coordinates		// Color Override	// Texture Coordinates
			p4.x, p4.y, p4.z,	1.0f, 1.0f, 1.0f,	1.0f, 1.0f,
			p3.x, p3.y, p3.z,	1.0f, 1.0f, 1.0f,	1.0f, 0.0f,
			p2.x, p2.y, p2.z,	1.0f, 1.0f, 1.0f,	0.0f, 0.0f,
			p1.x, p1.y, p1.z,	1.0f, 1.0f, 1.0f,	0.0f, 1.0f,
		};

		glGenBuffers(1, &_elementBufferObject);
		_drawIndices = new GLuint[6]{
			0, 1, 2,	// first triangle
			2, 3, 0		// second triangle
		};

		glGenVertexArrays(1, _vertexArrayObjects);
		glGenTextures(1, _textures);

		glBindVertexArray(*_vertexArrayObjects);

		/* Vertices Data */
		glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects);
		glBufferData(GL_ARRAY_BUFFER, _vertices_size * sizeof(GLfloat), _vertices, GL_STATIC_DRAW);

		/* Drawing Indices */
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _elementBufferObject);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), _drawIndices, GL_STATIC_DRAW);

		glBindVertexArray(NULL);
	}

	Rectangle::Rectangle(const GLfloat width, const GLfloat height, const glm::vec3& normal) {

		_normal = normal;
		_vertexBufferObjects = new GLuint[1];
		_vertexArrayObjects = new GLuint[1];
		_textures = new GLuint[1];
		glGenBuffers(1, _vertexBufferObjects);

		_vertices_size = 32;
		_vertices = new GLfloat[_vertices_size]
		{
			// Coordinates					// Colors			// Texture Coordinates
			width / 2, 0.0f, height / 2,	1.0f, 1.0f, 1.0f,	4.0f, 0.0f,
			width / 2, 0.0f, -height / 2,	1.0f, 1.0f, 1.0f,	4.0f, 4.0f,
			-width / 2, 0.0f, -height / 2,	1.0f, 1.0f, 1.0f,	0.0f, 4.0f,
			-width / 2, 0.0f, height / 2,	1.0f, 1.0f, 1.0f,	0.0f, 0.0f

		};

		glGenBuffers(1, &_elementBufferObject);
		_drawIndices = new GLuint[6]{
			0, 1, 2,	// first triangle
			2, 3, 0		// second triangle
		};

		glGenVertexArrays(1, _vertexArrayObjects);
		glGenTextures(1, _textures);

		glBindVertexArray(*_vertexArrayObjects);

		/* Vertices Data */
		glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects);
		glBufferData(GL_ARRAY_BUFFER, _vertices_size * sizeof(GLfloat), _vertices, GL_STATIC_DRAW);

		/* Drawing Indices */
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _elementBufferObject);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), _drawIndices, GL_STATIC_DRAW);

		glBindVertexArray(NULL);
	}

	Rectangle::~Rectangle() {
		glDeleteBuffers(1, _vertexBufferObjects);
		glDeleteBuffers(1, &_elementBufferObject);
		glDeleteVertexArrays(1, _vertexArrayObjects);
		glDeleteTextures(1, _textures);
		delete _drawIndices;
	}

	void Rectangle::onPreRender() {

		const GLuint uniNormalIndex = glGetUniformLocation(getProgram(), "normal");
		const GLuint uniViewPosIndex = glGetUniformLocation(getProgram(), "viewPos");
		const GLuint uniLightColorIndex = glGetUniformLocation(getProgram(), "lightColor");
		const GLuint uniLightPositionIndex = glGetUniformLocation(getProgram(), "lightPos");

		glUniform3fv(uniNormalIndex, 1, glm::value_ptr(getNormalVector()));
		if (_camera != nullptr) glUniform3fv(uniViewPosIndex, 1, glm::value_ptr(_camera->getPosition()));

		if (_lightSource != nullptr) {
			Color clr = _lightSource->getColor();
			Coordinate lp = _lightSource->getPosition();
			glUniform3f(uniLightColorIndex, clr.r, clr.g, clr.b);
			glUniform3f(uniLightPositionIndex, lp.x, lp.y, lp.z);
		}
		else {
			glUniform3f(uniLightColorIndex, 1.0f, 1.0f, 1.0f);
			glUniform3f(uniLightPositionIndex, 0.0f, 0.0f, 0.0f);
		}
		glUniform1i(glGetUniformLocation(getProgram(), "Texture"), 0);
		glUniform1i(glGetUniformLocation(getProgram(), "shadowMap"), 30);
	}

	void Rectangle::onRender() {

		const GLuint positionIndex = 0;
		const GLuint colorIndex = 1;
		const GLuint texIndex = 2;
		const GLsizei stride = 8 * sizeof(GLfloat);
		const GLuint uniMVPIndex = glGetUniformLocation(getProgram(), "mvp");
		const GLuint uniModelIndex = glGetUniformLocation(getProgram(), "model");

		/* Uniforms */
		glUniformMatrix4fv(uniMVPIndex, 1, GL_FALSE, glm::value_ptr(_mvpMat));
		glUniformMatrix4fv(uniModelIndex, 1, GL_FALSE, glm::value_ptr(_modelMat));
		glBindVertexArray(*_vertexArrayObjects);
			glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects); // very important!
		
			/* position */
			glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, stride, nullptr);
			glEnableVertexAttribArray(positionIndex);

			/* newColor_in */
			glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(colorIndex);

			/* texCoord_in */
			glVertexAttribPointer(texIndex, 2, GL_FLOAT, GL_FALSE, stride, (void*)(6 * sizeof(GLfloat)));
			glEnableVertexAttribArray(texIndex);

			/* Draw */
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, *_textures);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

			/* Clean up */
			glBindTexture(GL_TEXTURE_2D, NULL);
			glDisableVertexAttribArray(positionIndex);
			glDisableVertexAttribArray(colorIndex);
			glDisableVertexAttribArray(texIndex);
		glBindVertexArray(NULL);
	}

	void Rectangle::bindLightSource(LightSource * src) {
		_lightSource = src;
	}

	void Rectangle::unbindLightSource() {
		_lightSource = nullptr;
	}

	Building::Building(const Coordinate & upperLeft, const GLfloat width, const GLfloat height) {
		this->Mesh::translateTo(upperLeft);
		_abstractPosition = upperLeft;

		Coordinate start = upperLeft;
		_walls[0] = new Rectangle(
			upperLeft,
			upperLeft + glm::vec3(0.0f, height, 0.0f),
			upperLeft + glm::vec3(0.0f, height, width),
			upperLeft + glm::vec3(0.0f, 0.0f, width),
			glm::normalize(glm::cross(glm::vec3(0.0f, height, width), glm::vec3(0.0f, height, -width)))
		);
		_walls[0]->setName("Wall0");
		start = upperLeft + glm::vec3(0.0f, 0.0f, width);
		_walls[1] = new Rectangle(
			start,
			start + glm::vec3(0.0f, height, 0.0f),
			start + glm::vec3(width, height, 0.0f),
			start + glm::vec3(width, 0.0f, 0.0f),
			glm::normalize(glm::cross(glm::vec3(width, height, 0.0f), glm::vec3(-width, height, 0.0f)))
		);
		_walls[1]->setName("Wall1");
		start += glm::vec3(width, 0.0f, 0.0f);
		_walls[2] = new Rectangle(
			start,
			start + glm::vec3(0.0f, height, 0.0f),
			start + glm::vec3(0.0f, height, -width),
			start + glm::vec3(0.0f, 0.0f, -width),
			glm::normalize(glm::cross(glm::vec3(0.0f, height, -width),glm::vec3(0.0f, height, width)))
		);
		_walls[2]->setName("Wall2");
		start += glm::vec3(0.0f, 0.0f, -width);
		_walls[3] = new Rectangle(
			start,
			start + glm::vec3(0.0f, height, 0.0f),
			start + glm::vec3(-width, height, 0.0f),
			start + glm::vec3(-width, 0.0f, 0.0f),
			glm::normalize(glm::cross(glm::vec3(-width, height, 0.0f), glm::vec3(width, height, 0.0f)))
		);
		_walls[3]->setName("Wall3");
		_roof = new Rectangle(
			upperLeft + glm::vec3(0.0f, height, width),
			upperLeft + glm::vec3(0.0f, height, 0.0f),
			upperLeft + glm::vec3(width, height, 0.0f),
			upperLeft + glm::vec3(width, height, width),
			glm::vec3(0.0f, 1.0f, 0.0f)
		);
		_roof->setName("Roof");
	}

	Building::~Building() {
		for (int i = 0; i < 4; ++i) {
			delete _walls[i];
		}
		delete _roof;
	}

	void Building::bindCamera(Camera * cam) {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->bindCamera(cam);
		}
		_roof->bindCamera(cam);
	}

	void Building::unBindCamera() {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->unBindCamera();
		}
		_roof->unBindCamera();
	}

	void Building::bindShader(Shader * shader) {
		_shader = shader;
		for (int i = 0; i < 4; ++i) {
			_walls[i]->bindShader(shader);
		}
		_roof->bindShader(shader);
	}

	void Building::applyTexture(SDL_Surface * surface) {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->applyTexture(surface);
		}
		_roof->applyTexture(surface);
	}

	void Building::applyTexture(void * pixels, const int w, const int h) {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->applyTexture(pixels, w, h);
		}
		_roof->applyTexture(pixels, w, h);
	}

	void Building::translateTo(const Coordinate& dest) {
		this->Mesh::translateTo(dest);
		for (int i = 0; i < 4; ++i) {
			_walls[i]->translateTo(dest);
		}
		_roof->translateTo(dest);
	}

	void Building::translateBy(const float dist, const glm::vec3 & axis) {
		this->Mesh::translateBy(dist, axis);
		for (int i = 0; i < 4; ++i) {
			_walls[i]->translateBy(dist, axis);
		}
		_roof->translateBy(dist, axis);
	}

	void Building::rotateTo(const float deg, const glm::vec3 & axis) {
		this->Mesh::rotateTo(deg, axis);
		for (int i = 0; i < 4; ++i) {
			_walls[i]->rotateTo(deg, axis);
		}
		_roof->rotateTo(deg, axis);
	}

	void Building::rotateBy(const float deg, const glm::vec3 & axis) {
		this->Mesh::rotateBy(deg, axis);
		for (int i = 0; i < 4; ++i) {
			_walls[i]->rotateBy(deg, axis);
		}
		_roof->rotateBy(deg, axis);
	}

	void Building::onPreRender() {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->onPreRender();
		}
		_roof->onPreRender();
	}

	void Building::onRender() {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->render();
		}
		_roof->render();
	}

	void Building::renderSkip() {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->renderSkip();
		}
		_roof->renderSkip();
	}

	void Building::bindLightSource(LightSource * src) {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->bindLightSource(src);
		}
		_roof->bindLightSource(src);
	}

	void Building::unbindLightSource() {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->unbindLightSource();
		}
		_roof->unbindLightSource();
	}

	void Building::setLightSpaceMat(const glm::mat4 & mat) {
		for (int i = 0; i < 4; ++i) {
			_walls[i]->setLightSpaceMat(mat);
		}
		_roof->setLightSpaceMat(mat);
	}

	Coordinate Building::getPosition() {
		return glm::vec3(_viewMat * _modelMat * glm::vec4(_abstractPosition, 1.0f)) - _abstractPosition;
	}

	LightSource::LightSource(const Coordinate & pos, const Color& color) {
		_color = color;

		_vertexBufferObjects = new GLuint[1];
		_vertexArrayObjects = new GLuint[1];
		glGenBuffers(1, _vertexBufferObjects);

		_vertices_size = 108;
		_vertices = new GLfloat[_vertices_size]
		{
			-0.5f, -0.5f, 0.5f,
			-0.5f, 0.5f, 0.5f,
			0.5f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0.5f,
			0.5f, 0.5f, 0.5f,
			0.5f, -0.5f, 0.5f,
			
			0.5f, -0.5f, 0.5f,
			0.5f, 0.5f, 0.5f,
			0.5f, 0.5f, -0.5f,
			0.5f, -0.5f, 0.5f,
			0.5f, 0.5f, -0.5f,
			0.5f, -0.5f, -0.5f,

			
			0.5f, -0.5f, -0.5f,
			0.5f, 0.5f, -0.5f,
			-0.5f, -0.5f, -0.5f,
			0.5f, 0.5f, -0.5f,
			-0.5f, 0.5f, -0.5f,
			-0.5f, -0.5f, -0.5f,
			
			-0.5f, -0.5f, -0.5f,
			-0.5f, 0.5f, -0.5f,
			-0.5f, -0.5f, 0.5f,
			-0.5f, 0.5f, -0.5f,
			-0.5f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0.5f,
			
			-0.5f, 0.5f, 0.5f,
			-0.5f, 0.5f, -0.5f,
			0.5f, 0.5f, -0.5f,
			-0.5f, 0.5f, 0.5f,
			0.5f, 0.5f, -0.5f,
			0.5f, 0.5f, 0.5f,
						
			0.5f, -0.5f, -0.5f,
			-0.5f, -0.5f, -0.5f,
			-0.5f, -0.5f, 0.5f,
			0.5f, -0.5f, 0.5f,
			0.5f, -0.5f, -0.5f,
			-0.5f, -0.5f, 0.5f
		};

		glGenVertexArrays(1, _vertexArrayObjects);

		glBindVertexArray(*_vertexArrayObjects);

			/* Vertices Data */
			glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects);
			glBufferData(GL_ARRAY_BUFFER, _vertices_size * sizeof(GLfloat), _vertices, GL_STATIC_DRAW);

		glBindVertexArray(NULL);

		this->translateBy(glm::length(pos), glm::normalize(pos));
		_position = pos;
	}

	LightSource::~LightSource() {
		glDeleteBuffers(1, _vertexBufferObjects);
		glDeleteVertexArrays(1, _vertexArrayObjects);
	}

	void LightSource::applyTexture(SDL_Surface * surface) { /* NO-OP */ }
	void LightSource::applyTexture(void * pixels, const int w, const int h) { /* NO-OP */ }

	void LightSource::setColor(const Color & newColor) {
		_color = newColor;
	}

	Color LightSource::getColor() {
		return _color;
	}

	void LightSource::onPreRender(){ /* NO-OP */ }

	void LightSource::onRender() {

		const GLuint programID = getProgram();
		const GLuint positionIndex = 0;
		const GLuint uniMVPIndex = glGetUniformLocation(programID, "mvp");
		const GLuint uniCustomColorIndex = glGetUniformLocation(programID, "customColor");

		useShader();
		glBindVertexArray(*_vertexArrayObjects);

			glBindBuffer(GL_ARRAY_BUFFER, *_vertexBufferObjects); // very important!

			/* Uniforms */
			glUniformMatrix4fv(uniMVPIndex, 1, GL_FALSE, glm::value_ptr(_mvpMat));
			glUniform4f(uniCustomColorIndex, _color.r, _color.g, _color.b, _color.a);

			/* position */
			glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, NULL, nullptr);
			glEnableVertexAttribArray(positionIndex);

			/* Draw */
			glDrawArrays(GL_TRIANGLES, 0, 36);

			/* Clean up */
			glDisableVertexAttribArray(positionIndex);

		glBindVertexArray(NULL);
	}

}
