#ifndef CONNECTION_H
#define CONNECTION_H

#include <cinttypes>
#include <string>
#include <vector>

#include <WinSock2.h>
#include <WS2tcpip.h>

#include "SDL_thread.h"

class Connection {

	private:
		SOCKET _serverSocket = INVALID_SOCKET;
		SOCKET _clientSocket = INVALID_SOCKET;
		struct addrinfo *_hostInfo;
		bool _isListening = false;
		bool _isConnected = false;
		bool _isDataReady = false;
		bool _isDataChanged = false;

		char *_data;
		uint32_t _dataSize;
		SDL_Thread *_listeningThread;
		SDL_mutex *_mutex;
		uint16_t _port;
		uint32_t _width, _height, _type;

		std::string getIPFromSocket(const SOCKET socket);
		void setConnected(const bool con);
		void setListening(const bool lis);
		void setDataReady(const bool rdy);
		void setDataChanged(const bool chn);
		uint32_t recvType();
		uint32_t recvSize();
		uint32_t recvWidth();
		uint32_t recvHeight();
		bool recvImage(const uint32_t size);
		void startRecv();

	public:
		Connection(const uint16_t port);
		~Connection();
		
		bool isListening() const;
		bool isDataReady() const;
		bool isConnected() const;
		bool isDataChanged() const;
		void _internal_startListening();
		void startListening();
		void stopListening();
		void sendTestData();

		char *getData();
		uint32_t getType();
		uint32_t getSize();
		uint32_t getWidth();
		uint32_t getHeight();
};

#endif /* CONNECTION_H */
