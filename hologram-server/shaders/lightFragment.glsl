#version 440 core

out vec4 color;

uniform vec4 customColor;

void main() {
	color = customColor;
}