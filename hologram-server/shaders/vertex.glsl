#version 440 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 newColor_in;
layout (location = 2) in vec2 texCoord_in;

out vec3 newColor;
out vec2 texCoord;
out vec3 fragPos;
out vec3 normal_inter;
out vec4 fragPosLightSpace;

uniform mat4 mvp;
uniform mat4 model;
uniform vec3 normal;
uniform mat4 lightSpaceMat;

void main() {
	gl_Position = mvp * vec4(position, 1.0f);
	newColor = newColor_in;
	texCoord = texCoord_in;
	fragPos = vec3(model * vec4(position, 1.0f));
	normal_inter = normal;
	fragPosLightSpace = lightSpaceMat * vec4(fragPos, 1.0f);
}