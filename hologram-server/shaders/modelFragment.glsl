#version 440 core

out vec4 color;

in vec2 texCoord;
in vec3 fragPos;
in vec4 fragPosLightSpace;
in vec3 tangentLightPos;
in vec3 tangentViewPos;
in vec3 tangentFragPos;

uniform sampler2D texture_diffuse0;
uniform sampler2D texture_specular0;
uniform sampler2D texture_normal0;
uniform sampler2D shadowMap;

uniform vec3 lightColor;

float calculateShadowStrength(vec4 posLightSpace){
	vec3 projCoords = posLightSpace.xyz / posLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;

	if (projCoords.z > 1.0) return 0.0f;
	
	float closestDepth = texture(shadowMap, projCoords.xy).r; 
	vec2 texelSize = 1.0f / textureSize(shadowMap, 0);
	float currentDepth = projCoords.z;
	float bias = 0.001f;


	//return currentDepth - bias > closestDepth ? 1.0f : 0.0f;

	// PCF

	float shadow = 0.0f;
	for (int x = -1; x <= 1; ++x){
		for (int y = -1; y <= 1; ++y){
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0f : 0.0f;
		}
	}
	shadow /= 9.0f;
    return shadow;
}

void main() {

	vec3 textureColor = texture(texture_diffuse0, texCoord).rgb;
	
	vec3 normalColor = texture(texture_normal0, texCoord).rgb;
	normalColor = normalize(normalColor * 2.0 - 1.0);
	
	// Ambient
	float ambientStrength = 0.75f;
	vec3 ambient = ambientStrength * lightColor * textureColor;

	// Diffuse
	float diffuseStrength = 0.5f;
	vec3 lightDir = normalize(tangentLightPos - tangentFragPos);
	float diff = max(dot(normalColor, lightDir), 0.0);
	vec3 diffuse = diffuseStrength * diff * textureColor;

	// Specular
	float specularStrength = 0.5f;
	vec3 viewDir = normalize(tangentViewPos - tangentFragPos);
	//vec3 reflectDir = reflect(-lightDir, normalColor);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(normalColor, halfwayDir), 0.0), 64);
	vec3 specular = specularStrength * spec * lightColor; 

	// Shadow
	float shadow = calculateShadowStrength(fragPosLightSpace);
	vec3 color_rgb = ambient + (1.0f) * (diffuse + specular);

	// OpenCV uses BGR; reverse colors order
	color = vec4(color_rgb.z, color_rgb.y, color_rgb.x, 1.0f);
	
}