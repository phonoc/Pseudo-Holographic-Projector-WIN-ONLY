#version 440 core

out vec4 color;

in vec3 newColor;
in vec2 texCoord;
in vec3 fragPos;
in vec3 normal_inter;
in vec4 fragPosLightSpace;

uniform sampler2D Texture;
uniform sampler2D shadowMap;

uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;

float calculateShadowStrength(vec4 posLightSpace){
	vec3 projCoords = posLightSpace.xyz / posLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;

	if (projCoords.z > 1.0) return 0.0f;
	
	float closestDepth = texture(shadowMap, projCoords.xy).r; 
	vec2 texelSize = 1.0f / textureSize(shadowMap, 0);
	float currentDepth = projCoords.z;
	float bias = 0.000f;
	//return currentDepth - bias > closestDepth ? 1.0f : 0.0f;

	// PCF
	float shadow = 0.0f;
	for (int x = -1; x <= 1; ++x){
		for (int y = -1; y <= 1; ++y){
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0f : 0.0f;
		}
	}
	shadow /= 9.0f;
    return shadow;
}

void main() {

	// Ambient
	float ambientStrength = 0.6f;
	vec3 ambient = ambientStrength * lightColor;

	// Diffuse
	float diffuseStrength = 0.3f;
	vec3 norm = normalize(normal_inter);
	vec3 lightDir = normalize(lightPos - fragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diffuseStrength * diff * lightColor;

	// Specular
	float specularStrength = 0.2f;
	vec3 viewDir = normalize(viewPos - fragPos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 4);
	vec3 specular = specularStrength * spec * lightColor; 

	// Shadow
	float shadow = calculateShadowStrength(fragPosLightSpace);

	color = vec4((ambient + (1.0f - shadow) * (diffuse + specular)) * newColor * vec3(texture(Texture, texCoord)), texture(Texture, texCoord).a);
}