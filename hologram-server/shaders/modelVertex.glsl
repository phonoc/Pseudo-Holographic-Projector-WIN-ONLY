#version 440 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord_in;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out vec2 texCoord;
out vec3 fragPos;
out vec4 fragPosLightSpace;
out vec3 tangentLightPos;
out vec3 tangentViewPos;
out vec3 tangentFragPos;

uniform mat4 mvp;
uniform mat4 model;
uniform mat4 lightSpaceMat;

uniform vec3 lightPos;
uniform vec3 viewPos;

void main() {

	gl_Position = mvp * vec4(position, 1.0f);

	mat3 normalMat = transpose(inverse(mat3(model)));

	vec3 T = normalize(normalMat * tangent);
	vec3 B = normalize(normalMat * bitangent);
	vec3 N = normalize(normalMat * normal);

	if (dot(cross(N, T), B) < 0.0)
                T = T * -1.0;

	texCoord = texCoord_in;
	fragPos = vec3(model * vec4(position, 1.0f));
	fragPosLightSpace = lightSpaceMat * vec4(fragPos, 1.0f);

	mat3 TBN = mat3(T, B, N);

	tangentLightPos = lightPos * TBN;
    tangentViewPos  = viewPos * TBN;
    tangentFragPos  = fragPos * TBN;
}