#include "camera.hpp"
#include "glm\gtc\matrix_transform.hpp"

Camera::Camera(const glm::vec3 & pos, const glm::vec3 & target) {
	_position = pos;
	_target = target;
	updateValues();
}

void Camera::updateValues() {
	_direction = glm::normalize(_target - _position);
	_lookAt = lookAt(_target);
}

void Camera::moveTo(const glm::vec3 & pos) {
	_position = pos;
	updateValues();
}

void Camera::moveBy(const glm::vec3 & dist) {
	_position += dist;
	_target += dist;
	updateValues();
}

void Camera::rotateBy(const float deg, const glm::vec3& axis) {
	glm::vec4 mult = glm::vec4(_position.x, _position.y, _position.z, 1.0f);
	_position = glm::rotate(glm::mat4(), glm::radians(deg), axis) * mult;
	
	mult = glm::vec4(_target.x, _target.y, _target.z, 1.0f);
	_target = glm::rotate(glm::mat4(), glm::radians(deg), axis) * mult;

	updateValues();
}

void Camera::rotateYBy(const float deg) {
	rotateBy(deg, glm::vec3(0.0f, 1.0f, 0.0f));
}

void Camera::rotateUp(const float deg) {
	rotateBy(deg, glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), _direction)));
}

void Camera::rotateDown(const float deg) {
	rotateBy(-deg, glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), _direction)));
}

void Camera::moveForward(const float dist) {
	glm::vec3 distVec = dist * _direction;
	_position += distVec;
	_target += distVec;
	updateValues();
}

void Camera::moveBackward(const float dist) {
	glm::vec3 distVec = dist * -1.0f * _direction;
	_position += distVec;
	_target += distVec;
	updateValues();
}

void Camera::moveLeft(const float dist) {
	glm::vec3 distVec = dist * glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), _direction));
	_position += distVec;
	_target += distVec;
	updateValues();
}

void Camera::moveRight(const float dist) {
	glm::vec3 distVec = -1.0f * dist * glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), _direction));
	_position += distVec;
	_target += distVec;
	updateValues();
}

void Camera::moveUp(const float dist) {
	moveBy(dist * glm::vec3(0.0f, 1.0f, 0.0f));
}

void Camera::moveDown(const float dist) {
	moveBy(-1.0f * dist * glm::vec3(0.0f, 1.0f, 0.0f));
}

glm::mat4 Camera::lookAt(const glm::vec3& target) {
	return _lookAt = glm::lookAt(_position, target, glm::vec3(0.0f, 1.0f, 0.0f));
}

glm::mat4 Camera::getTranslationMat() {
	return _lookAt;
}

glm::vec3 Camera::getPosition() {
	return _position;
}
