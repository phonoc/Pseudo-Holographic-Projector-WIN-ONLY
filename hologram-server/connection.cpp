#include "SDL_thread.h"
#include "SDL.h"
#include "connection.hpp"
#include "logger.hpp"
#include "config.hpp"

Connection::Connection(const uint16_t port) {

	_hostInfo = nullptr;
	_port = port;
	_mutex = SDL_CreateMutex();
	_data = nullptr;

	if (_mutex == nullptr) {
		logger::putError("Could not create mutex: " + std::string(SDL_GetError()));
		std::exit(EXIT_FAILURE);
	}

	struct addrinfo hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	/* Resolve local address */
	int initResult = GetAddrInfo(nullptr, std::to_string(port).c_str(), &hints, &_hostInfo);
	if (initResult) {
		logger::putError("Could not resolve local host address");
		std::exit(EXIT_FAILURE);
	}

	/* Create socket */
	_serverSocket = socket(_hostInfo->ai_family, _hostInfo->ai_socktype, _hostInfo->ai_protocol);
	if (_serverSocket == INVALID_SOCKET) {
		logger::putError("Unable to create socket");
		std::exit(EXIT_FAILURE);
	}

	/* Bind socket */
	initResult = bind(_serverSocket, _hostInfo->ai_addr, _hostInfo->ai_addrlen);
	if (initResult == SOCKET_ERROR) {
		logger::putError("Binding socket to port " + std::to_string(port) + " failed.");
		std::exit(EXIT_FAILURE);
	}
}

Connection::~Connection() {

	if (_hostInfo != nullptr) {
		freeaddrinfo(_hostInfo);
	}
	if (_serverSocket != INVALID_SOCKET) {
		shutdown(_serverSocket, SD_BOTH);
		closesocket(_serverSocket);
	}
	if (_data != nullptr)
		delete[] _data;
}

std::string Connection::getIPFromSocket(const SOCKET socket) {

	struct sockaddr_in sockInfo;
	int nameLen = sizeof(sockaddr_in);
	char ip[INET_ADDRSTRLEN];

	getpeername(socket, (sockaddr *) &sockInfo, &nameLen);
	inet_ntop(AF_INET, &sockInfo.sin_addr, ip, INET_ADDRSTRLEN);

	return std::string(ip);
}

void Connection::setConnected(const bool con) {
	SDL_LockMutex(_mutex);
	_isConnected = con;
	SDL_UnlockMutex(_mutex);
}

void Connection::setListening(const bool lis) {
	SDL_LockMutex(_mutex);
	_isListening = lis;
	SDL_UnlockMutex(_mutex);
}

void Connection::setDataReady(const bool rdy) {
	SDL_LockMutex(_mutex);
	_isDataReady = rdy;
	SDL_UnlockMutex(_mutex);
}

void Connection::setDataChanged(const bool chn) {
	SDL_LockMutex(_mutex);
	_isDataChanged = chn;
	SDL_UnlockMutex(_mutex);
}

uint32_t Connection::recvType() {
	uint32_t ret = 0;
	char *offset = (char *) &ret;

	uint64_t remaining = sizeof(uint32_t);
	int buf_recv = 0;
	while (remaining > 0) {
		buf_recv = recv(_clientSocket, offset, sizeof(uint32_t), MSG_WAITALL);

		if (buf_recv <= 0) {  /* Peer disconnects. */
			setListening(false);
			break;
		}

		SDL_LockMutex(_mutex);
		offset += buf_recv;
		SDL_UnlockMutex(_mutex);

		setDataReady(false);
		remaining -= buf_recv;
	}
	return ret;
}

uint32_t Connection::recvSize() {
	return recvType();
}

uint32_t Connection::recvWidth() {
	return recvType();
}

uint32_t Connection::recvHeight() {
	return recvType();
}

bool Connection::recvImage(const uint32_t size) {
	logger::putInfo("Receiving image data (size = " + std::to_string(size) + " bytes)");
	if (size < 1) return EXIT_FAILURE;
	if (_data != nullptr) delete[] _data; // clean old data
	_data = new char[size];

	char *offset = _data;
	uint32_t remaining = size;
	int buf_recv = 0;
	while (remaining > 0) {
		SDL_LockMutex(_mutex);
		buf_recv = recv(_clientSocket, offset, remaining, MSG_WAITALL);
		SDL_UnlockMutex(_mutex);

		if (buf_recv < 0) { /* Transmission failed. Try again */
			continue;
		}

		if (buf_recv == 0) {  /* Peer disconnects. */
			setListening(false);
			return EXIT_FAILURE;
		}
		setDataReady(false);
		remaining -= buf_recv;
		offset += buf_recv;
	}

	return EXIT_SUCCESS;
}

void Connection::_internal_startListening() {

	logger::putInfo("Listening for connection...");

	int initResult = listen(_serverSocket, SOMAXCONN);
	if (initResult == SOCKET_ERROR) {
		logger::putError("Unable to listen for connection: code " + std::to_string(WSAGetLastError()));
		std::exit(EXIT_FAILURE);
	}

	/* Somebody connects to the server */
	_clientSocket = accept(_serverSocket, nullptr, nullptr);

	if (_clientSocket == INVALID_SOCKET) {
		logger::putError("Unable to accept connection: code " + std::to_string(WSAGetLastError()));
		std::exit(EXIT_FAILURE);
	}

	logger::putInfo("Accepted connection from " + getIPFromSocket(_clientSocket));

	/* No error so far, start data transmission */
	setConnected(true);
	startRecv();
	setConnected(false);
	logger::putInfo("Connection closed.");
	setListening(false);
}

void Connection::startRecv() {
	while (this->isListening()){
		
		bool result = true;

		_type = recvType();
		if (_type == 0) {
			logger::putInfo("Received termination opcode (0)");
			this->setListening(false);
			this->setDataChanged(false);
			this->setDataReady(false);
			break;
		}

		_dataSize = recvSize();
		_width = recvWidth();
		_height = recvHeight();
		result = recvImage(_dataSize);

		if (result == EXIT_SUCCESS) {
			setDataReady(true);
			setDataChanged(true);
		}
	}
}

bool Connection::isListening() const {
	SDL_LockMutex(_mutex);
	bool ret = _isListening;
	SDL_UnlockMutex(_mutex);
	return ret;
}

bool Connection::isDataReady() const {
	SDL_LockMutex(_mutex);
	bool ret = _isDataReady;
	SDL_UnlockMutex(_mutex);
	return ret;
}

bool Connection::isConnected() const {
	SDL_LockMutex(_mutex);
	bool ret = _isConnected;
	SDL_UnlockMutex(_mutex);
	return ret;
}

bool Connection::isDataChanged() const {
	SDL_LockMutex(_mutex);
	bool ret = _isDataChanged;
	SDL_UnlockMutex(_mutex);
	return ret;
}

static int listenerThread(void *connection) {
	Connection *con_instance = static_cast<Connection*>(connection);
	con_instance->_internal_startListening();
	return EXIT_SUCCESS;
}

void Connection::startListening() {
	setListening(true);
	_listeningThread = SDL_CreateThread(listenerThread, nullptr, this);
}

void Connection::stopListening() {
	
	if (!isConnected()) {

		/* Connect to itself so that listen() can finish. */
		struct addrinfo hints, *svInfo;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		/* Resolve local address */
		int initResult = GetAddrInfo("127.0.0.1", std::to_string(_port).c_str(), &hints, &svInfo);
		if (initResult) {
			logger::putError("Could not resolve local host address");
			std::exit(EXIT_FAILURE);
		}

		/* Create socket */
		SOCKET dummySocket = socket(svInfo->ai_family, svInfo->ai_socktype, svInfo->ai_protocol);
		if (dummySocket == INVALID_SOCKET) {
			logger::putError("Unable to create socket");
			std::exit(EXIT_FAILURE);
		}
		int res = connect(dummySocket, svInfo->ai_addr, (int) svInfo->ai_addrlen);
		if (res == INVALID_SOCKET) logger::putError("Could not connect");

		uint32_t opcode = 0;
		send(dummySocket, (char*)&opcode, sizeof(uint32_t), 0);

		closesocket(dummySocket);
		shutdown(dummySocket, SD_BOTH);

	}
	else {
		shutdown(_clientSocket, SD_RECEIVE);
		closesocket(_clientSocket);
	}

	int status;
	SDL_WaitThread(_listeningThread, &status);
}

void Connection::sendTestData() {

	/* Connect to itself so that listen() can finish. */
	struct addrinfo hints, *svInfo;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	/* Resolve local address */
	int initResult = GetAddrInfo("127.0.0.1", std::to_string(_port).c_str(), &hints, &svInfo);
	if (initResult) {
		logger::putError("Could not resolve local host address");
		std::exit(EXIT_FAILURE);
	}

	/* Create socket */
	SOCKET dummySocket = socket(svInfo->ai_family, svInfo->ai_socktype, svInfo->ai_protocol);
	if (dummySocket == INVALID_SOCKET) {
		logger::putError("Unable to create socket");
		std::exit(EXIT_FAILURE);
	}
	int res = connect(dummySocket, svInfo->ai_addr, (int)svInfo->ai_addrlen);
	if (res == INVALID_SOCKET) logger::putError("Could not connect");

	const char *dummyPacket = "123456789";
	int bsent = 0;
	while (bsent < 10 * sizeof(char)) {
		int bsent_r = send(dummySocket, dummyPacket, 10 * sizeof(char), 0);
		if (bsent_r == SOCKET_ERROR) {
			logger::putError("Unable to send data: code " + std::to_string(WSAGetLastError()));
		}
		bsent += bsent_r;
	}

	closesocket(dummySocket);
	shutdown(dummySocket, SD_BOTH);
}

char* Connection::getData() {

	if (!isDataReady()) return nullptr;

	setDataChanged(false);
	SDL_LockMutex(_mutex);
	char * ret = _data;
	SDL_UnlockMutex(_mutex);
	return ret;
}

uint32_t Connection::getType() {
	return _type;
}

uint32_t Connection::getSize() {
	return _dataSize;
}

uint32_t Connection::getWidth()
{
	return _width;
}

uint32_t Connection::getHeight() {
	return _height;
}
