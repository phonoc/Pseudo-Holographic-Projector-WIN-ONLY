#include "model.hpp"
#include "config.hpp"
#include "logger.hpp"
#include "image.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"

MeshModel::MeshModel(const std::vector<Vertex>& vv, const std::vector<GLuint>& iv, const std::vector<Texture>& tv) {
	this->vertices = vv;
	this->indices = iv;
	this->textures = tv;
}

MeshModel::~MeshModel() {
	// DO NOT DELETE BUFFERS
}

void MeshModel::onRender(const glm::mat4 & mvp, const glm::mat4 & model) {

	const GLuint uniMVPIndex = glGetUniformLocation(getProgram(), "mvp");
	const GLuint uniModelIndex = glGetUniformLocation(getProgram(), "model");
	glUniformMatrix4fv(uniMVPIndex, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniformMatrix4fv(uniModelIndex, 1, GL_FALSE, glm::value_ptr(model));

	glBindBuffer(GL_ARRAY_BUFFER, this->_vertexBufferObject);

	GLuint diffuseNr = 0;
	GLuint specularNr = 0;
	GLuint normalNr = 0;
	for (GLuint i = 0; i < this->textures.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i); // Activate proper texture unit before binding

		std::string name = this->textures[i].type;
		std::string number;
		if (name == "texture_diffuse")
			number = std::to_string(diffuseNr++);
		else if (name == "texture_specular")
			number = std::to_string(specularNr++);
		else if (name == "texture_normal")
			number = std::to_string(normalNr++);

		glUniform1i(glGetUniformLocation(_shader->getProgram(), (name + number).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
	}

	// Draw mesh
	glBindVertexArray(this->_vertexArrayObject);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(NULL);

	for (GLuint i = 0; i < this->textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void MeshModel::onPreRender() {

	const GLuint uniViewPosIndex = glGetUniformLocation(getProgram(), "viewPos");
	const GLuint uniLightColorIndex = glGetUniformLocation(getProgram(), "lightColor");
	const GLuint uniLightPositionIndex = glGetUniformLocation(getProgram(), "lightPos");

	glUniform3fv(uniViewPosIndex, 1, glm::value_ptr(_camera->getPosition()));
	if (_lightSource != nullptr) {
		Color clr = _lightSource->getColor();
		Coordinate lp = _lightSource->getPosition();
		glUniform3f(uniLightColorIndex, clr.r, clr.g, clr.b);
		glUniform3f(uniLightPositionIndex, lp.x, lp.y, lp.z);
	}
	else {
		glUniform3f(uniLightColorIndex, 1.0f, 1.0f, 1.0f);
		glUniform3f(uniLightPositionIndex, 0.0f, 0.0f, 0.0f);
	}
	
}

GLuint MeshModel::getProgram() {
	return _shader->getProgram();
}

void MeshModel::bindCamera(Camera * cam) {
	_camera = cam;
}

void MeshModel::unBindCamera() {
	_camera = nullptr;
}

void MeshModel::bindShader(Shader * shader) {
	_shader = shader;
}

void MeshModel::bindLightSource(Mesh::LightSource * src) {
	_lightSource = src;
}

void MeshModel::setup() {
	glGenVertexArrays(1, &_vertexArrayObject);
	glGenBuffers(1, &_vertexBufferObject);
	glGenBuffers(1, &_elementBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	const GLuint positionIndex = 0;
	const GLuint normalIndex = 1;
	const GLuint texIndex = 2;
	const GLuint tangentIndex = 3;
	const GLuint bitangentIndex = 4;
	const GLsizei stride = sizeof(Vertex);

	// Vertex Positions
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, stride, nullptr);

	// Vertex Normals
	glEnableVertexAttribArray(normalIndex);
	glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, stride, (void *)offsetof(Vertex, normal));

	// Vertex Texture Coords
	glEnableVertexAttribArray(texIndex);
	glVertexAttribPointer(texIndex, 2, GL_FLOAT, GL_FALSE, stride, (void *)offsetof(Vertex, texCoords));

	// Vertex Tangents
	glEnableVertexAttribArray(tangentIndex);
	glVertexAttribPointer(tangentIndex, 3, GL_FLOAT, GL_FALSE, stride, (void *)offsetof(Vertex, tangent));

	// Vertex Tangents
	glEnableVertexAttribArray(bitangentIndex);
	glVertexAttribPointer(bitangentIndex, 3, GL_FLOAT, GL_FALSE, stride, (void *)offsetof(Vertex, bitangent));

	glBindVertexArray(NULL);
}

void MeshModel::clean() {
	glDeleteBuffers(1, &_vertexBufferObject);
	glDeleteBuffers(1, &_elementBufferObject);
	glDeleteVertexArrays(1, &_vertexArrayObject);
}

void MeshModel::render(const glm::mat4 & mvp, const glm::mat4 model) {
	this->_shader->use();
	this->onPreRender();
	this->onRender(mvp, model);
}

void MeshModel::renderSkip(const glm::mat4 & mvp, const glm::mat4 model) {
	this->_shader->use();
	this->onRender(mvp, model);
}

Model::Model(const std::string & path) {
	
	_asmPixels = nullptr;
	_abstractPosition = { 0.0f, 0.0f, 0.0f };
	_shader = nullptr;
	_camera = nullptr;

	_viewMat = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));
	_modelMat = glm::rotate(glm::mat4(), 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	_projectionMat = glm::perspective(
		45.0f,
		(float) CONFIG::GET_WIN_WIDTH() / (float)CONFIG::GET_WIN_HEIGHT(),
		0.1f,
		100.0f
	);
	_directory = "model/" + path.substr(0, path.find_last_of('/'));
	updateMVP();

	loadModel(path);
}

Model::Model(const std::string & model, void * asm_pixels, const int w, const int h) {

	_asmPixels = (char *) asm_pixels;
	_asmWidth = w;
	_asmHeight = h;

	_abstractPosition = { 0.0f, 0.0f, 0.0f };
	_shader = nullptr;
	_camera = nullptr;

	_viewMat = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));
	_modelMat = glm::rotate(glm::mat4(), 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	_projectionMat = glm::perspective(
		45.0f,
		(float)CONFIG::GET_WIN_WIDTH() / (float)CONFIG::GET_WIN_HEIGHT(),
		0.1f,
		100.0f
	);
	_directory = "model/" + model.substr(0, model.find_last_of('/'));
	updateMVP();

	loadModel(model);
}

Model::Model(const std::string & path_to_obj, void * asm_png_data, const uint32_t data_size) {

	_asmPixels = (char *) asm_png_data;
	_asmSize = data_size;
	_abstractPosition = { 0.0f, 0.0f, 0.0f };
	_shader = nullptr;
	_camera = nullptr;

	_viewMat = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));
	_modelMat = glm::rotate(glm::mat4(), 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	_projectionMat = glm::perspective(
		45.0f,
		(float)CONFIG::GET_WIN_WIDTH() / (float)CONFIG::GET_WIN_HEIGHT(),
		0.1f,
		100.0f
	);
	_directory = "model/" + path_to_obj.substr(0, path_to_obj.find_last_of('/'));
	updateMVP();

	loadModel(path_to_obj);
}

void Model::updateMVP() {
	if (_camera != nullptr) {
		_mvpMat = _projectionMat * (_camera->getTranslationMat() * _viewMat) * _modelMat;
	}
	else {
		_mvpMat = _projectionMat * _viewMat * _modelMat;
	}
}

void Model::useShader() const {
	if (_shader != nullptr) {
		_shader->use();
	}
	else {
		logger::putWarning(_name + ": Using null shader, this may lead into problems!");
	}
}

void Model::loadModel(const std::string & path) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile("model/" + path,
		aiProcess_GenSmoothNormals | aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::exit(logger::putError("Assimp: " + std::string(importer.GetErrorString())));
	}
	this->processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode * node, const aiScene * scene) {
	
	// Process all the node's meshes (if any)
	for (GLuint i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->_meshes.push_back(this->processMesh(mesh, scene));
	}

	// Then do the same for each of its children
	for (GLuint i = 0; i < node->mNumChildren; i++) {
		this->processNode(node->mChildren[i], scene);
	}
}

GLuint Model::loadTextureFromPixels(void * pixels, const int w, const int h) {
	GLuint textureID;
	glGenTextures(1, &textureID);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGB,
		w,
		h,
		0,
		GL_BGR,
		GL_UNSIGNED_BYTE,
		pixels
	);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

GLuint Model::loadTextureFromFileRW(char *pngData, const uint32_t dataSize) {
	SDL_RWops *rw = SDL_RWFromMem(pngData, dataSize);
	SDL_Surface * png = IMG_LoadPNG_RW(rw);
	SDL_RWclose(rw);
	GLuint ret = loadTextureFromPixels(png->pixels, png->w, png->h);
	SDL_FreeSurface(png);
	return ret;
}

GLuint Model::loadTextureFromFile(const char *fname, const std::string & dir) {

	//Generate texture ID and load texture data 
	std::string filename = dir + '/' + fname;

	Image *image = new Image(filename);
	SDL_Surface  * imageSurface = image->getSurface();
	GLuint textureID = loadTextureFromPixels(imageSurface->pixels, imageSurface->w, imageSurface->h);
	SDL_FreeSurface(imageSurface);
	return textureID;
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, std::string typeName) {
	std::vector<Texture> textures;
	for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {

		aiString str;
		mat->GetTexture(type, i, &str);

		Texture texture;
		bool skip = false;
		if (_asmPixels == nullptr) {
			// Load texture from file
			for (int i = 0; i < _loadedTextures.size(); ++i) {
				if (str == _loadedTextures[i].path) {
					textures.push_back(texture);
					skip = true;
				}
			}
			if (skip) continue;
			texture.id = loadTextureFromFile(str.C_Str(), this->_directory);
			texture.type = typeName;
			texture.path = str;
			_loadedTextures.push_back(texture);
			logger::putInfo("Loaded texture from '" + std::string(str.C_Str()) + "'");
			textures.push_back(texture);
		}
		else {
			// Load texture from pre-loaded pixels
			if (!_asmTextureReady) {
				//texture.id = loadTextureFromPixels(_asmPixels, _asmWidth, _asmHeight);
				//texture.id = loadTextureFromFile("temp.png", _directory);
				texture.id = loadTextureFromFileRW(_asmPixels, _asmSize);
				texture.type = type;
				textures.push_back(texture);
				logger::putInfo("Loaded assembly texture");
				_loadedAsmTexture = texture.id;
				_asmTextureReady = true;
			}
			else {
				texture.id = _loadedAsmTexture;
				logger::putInfo("Loaded pre-loaded assembly texture");
			}
		}
	}
	return textures;
}

MeshModel Model::processMesh(aiMesh * mesh, const aiScene * scene) {
	
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
		
		// Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;

		// Normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.normal = vector;

		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		}
		else
			vertex.texCoords = glm::vec2(0.0f, 0.0f);

		// Tangent
		vector.x = mesh->mTangents[i].x;
		vector.y = mesh->mTangents[i].y;
		vector.z = mesh->mTangents[i].z;
		vertex.tangent = vector;

		// Bitangent
		vector.x = mesh->mBitangents[i].x;
		vector.y = mesh->mBitangents[i].y;
		vector.z = mesh->mBitangents[i].z;
		vertex.bitangent = vector;

		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for (GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		std::vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
	}

	// Return a mesh object created from the extracted mesh data
	MeshModel ret = MeshModel(vertices, indices, textures);
	ret.setup();
	return ret;
}

void Model::bindCamera(Camera * cam) {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].bindCamera(cam);
	}
	_camera = cam;
}

void Model::unBindCamera() {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].unBindCamera();
	}
	_camera = nullptr;
}

void Model::bindShader(Shader * shader) {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].bindShader(shader);
	}
	_shader = shader;
}

void Model::bindLightSource(Mesh::LightSource * src) {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].bindLightSource(src);
	}
	_lightSource = src;
}

void Model::translateTo(const Coordinate& dest) {
	glm::mat4 transMat = glm::translate(glm::mat4(), dest);
	_viewMat = transMat;
	updateMVP();
}

void Model::translateBy(const float dist, const glm::vec3 & axis) {
	glm::mat4 transMat = glm::translate(glm::mat4(), dist * axis);
	_viewMat = _viewMat * transMat;
	updateMVP();
}

void Model::rotateTo(const float deg, const glm::vec3& axis) {
	_rotate = deg;
	_modelMat = glm::rotate(glm::mat4(), glm::radians(_rotate), axis);
	updateMVP();
}

void Model::rotateBy(float deg, const glm::vec3& axis) {
	_rotate += deg;
	if (_rotate < -360.0f) {
		_rotate += -360.0f;
	}
	else if (_rotate > 360.0f) {
		_rotate -= 360.0f;
	}
	rotateTo(_rotate, axis);
}

Shader * Model::getShader() {
	return _shader;
}

void Model::clean() {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].clean();
	}
}

void Model::render() {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].render(_mvpMat, _modelMat);
	}
}

void Model::renderSkip() {
	for (int i = 0; i < _meshes.size(); ++i) {
		_meshes[i].renderSkip(_mvpMat, _modelMat);
	}
}
