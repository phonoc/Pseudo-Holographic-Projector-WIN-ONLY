#include "shadowmap.hpp"
#include "config.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

ShadowMap::ShadowMap(Mesh::LightSource * lsrc) {
	_lightSource = lsrc;
	_shadowShader = new Shader("shaders/shadowVertex.glsl", "shaders/shadowFragment.glsl");

	glGenFramebuffers(1, &_depthMapFrameBufferObject);
	glBindFramebuffer(GL_FRAMEBUFFER, _depthMapFrameBufferObject);
	glGenTextures(1, &_depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, _depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, NULL, GL_DEPTH_COMPONENT, SHADOWMAP_WIDTH, SHADOWMAP_HEIGHT, NULL, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthMapTexture, NULL);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, NULL);
}

ShadowMap::~ShadowMap() {
	delete _shadowShader;
	glDeleteFramebuffers(1, &_depthMapFrameBufferObject);
	glDeleteTextures(1, &_depthMapTexture);
}
	
void ShadowMap::bindLightSource(Mesh::LightSource * src) {
	_lightSource = src;
}

void ShadowMap::unbindLightSource() {
	_lightSource = nullptr;
}

void ShadowMap::bindMesh(Mesh::Mesh * mesh) {
	_meshes.insert(mesh);
}

int ShadowMap::unbindMesh(Mesh::Mesh * mesh) {
	return _meshes.erase(mesh);
}

void ShadowMap::bindModel(Model * model) {
	_models.insert(model);
}

int ShadowMap::unbindModel(Model * model) {
	return _models.erase(model);
}

void ShadowMap::render() {

	const GLfloat near_plane = -10.0f, far_plane = 60.0 * std::sqrt(2.0f);
	const glm::mat4 lightProjectionMat = glm::ortho(-60.0f, 60.0f, -60.0f, 60.0f, near_plane, far_plane);
	const glm::mat4 lightViewMat = glm::lookAt(_lightSource->getPosition(), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	const glm::mat4 lightSpaceMat = lightProjectionMat * lightViewMat;

	glCullFace(GL_FRONT);
	_shadowShader->use();
	glUniformMatrix4fv(glGetUniformLocation(_shadowShader->getProgram(), "lightSpaceMat"), 1, GL_FALSE, glm::value_ptr(lightSpaceMat));
	glViewport(0, 0, SHADOWMAP_WIDTH, SHADOWMAP_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, _depthMapFrameBufferObject);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	/* Draw simple primitives' shadow depth */
	for (std::unordered_set<Mesh::Mesh *>::iterator it = _meshes.begin(); it != _meshes.end(); ++it) {
		Shader * oldShader = (*it)->getShader();
		oldShader->use();
		glActiveTexture(GL_TEXTURE30);
		glBindTexture(GL_TEXTURE_2D, _depthMapTexture);
		glUniformMatrix4fv(glGetUniformLocation(oldShader->getProgram(), "lightSpaceMat"), 1, GL_FALSE, glm::value_ptr(lightSpaceMat));
		(*it)->bindShader(_shadowShader);
		(*it)->renderSkip(); // do not perform pre-render light uniform feeding (ffs!)
		(*it)->bindShader(oldShader);
	}	

	/* Draw complex models' shadow depth */
	for (std::unordered_set<Model *>::iterator it = _models.begin(); it != _models.end(); ++it) {
		Shader * oldShader = (*it)->getShader();
		oldShader->use();
		glUniform1i(glGetUniformLocation(oldShader->getProgram(), "shadowMap"), 30);
		glActiveTexture(GL_TEXTURE30);
		glBindTexture(GL_TEXTURE_2D, _depthMapTexture);
		glUniformMatrix4fv(glGetUniformLocation(oldShader->getProgram(), "lightSpaceMat"), 1, GL_FALSE, glm::value_ptr(lightSpaceMat));
		(*it)->bindShader(_shadowShader);
		(*it)->renderSkip(); // do not perform pre-render light uniform feeding (ffs!)
		(*it)->bindShader(oldShader);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, NULL);
	glViewport(0, 0, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT());
	glCullFace(GL_BACK);
}
