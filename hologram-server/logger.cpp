#include <ctime>

#include "SDL_thread.h"

#include "logger.hpp"

namespace logger {

	std::ofstream _logStream;
	std::string _logFile;
	bool _isMonOut = false;
	bool _lock = false;
	SDL_mutex *_mutex;

	std::string getCurrentTimeString() {
		time_t timer = time(nullptr);
		struct tm timeInfo;
		localtime_s(&timeInfo, &timer);
		char tmp[31];
		strftime(tmp, 31, "%T", &timeInfo);
		return std::string(tmp);
	}

	bool putError(const std::string& msg) {
		SDL_LockMutex(_mutex);
		_logStream << "(" << getCurrentTimeString() << ") [ERROR] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [ERROR] " + msg << std::endl;
		SDL_UnlockMutex(_mutex);
		return false;
	}

	void putWarning(const std::string& msg) {
		SDL_LockMutex(_mutex);
		_logStream << "(" << getCurrentTimeString() << ") [WARNING] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [WARNING] " + msg << std::endl;
		SDL_UnlockMutex(_mutex);
	}

	void putInfo(const std::string& msg) {
		SDL_LockMutex(_mutex);
		_logStream << "(" << getCurrentTimeString() << ") [INFO] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [INFO] " + msg << std::endl;
		SDL_UnlockMutex(_mutex);
	}

	void start(const std::string& fileName) {
		_logFile = fileName;
		_logStream.open(fileName.c_str(), _logStream.out);
		_mutex = SDL_CreateMutex();
		if (_mutex == nullptr) {
			putError("Could not create mutex: " + std::string(SDL_GetError()));
			std::exit(EXIT_FAILURE);
		}
	}

	void stop() {
		_logStream << "(" << getCurrentTimeString() << ") [INFO] ====== END OF LOG ======" << std::endl;
		_logStream.close();
		if (_mutex != nullptr) SDL_DestroyMutex(_mutex);
	}

	void setMonOut(const bool out) {
		_isMonOut = out;
	}

} /* namespace logger */