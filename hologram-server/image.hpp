#ifndef IMAGE_H
#define IMAGE_H

#include <string>

#include "SDL_image.h"

class Image {

	private:
		SDL_Surface *_imageSurface;

	public:
		Image(const std::string& fpath);
		~Image();

		SDL_Surface *getSurface();

};

#endif /* IMAGE_H */
