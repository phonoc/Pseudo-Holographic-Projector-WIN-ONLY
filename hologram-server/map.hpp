#ifndef MAP_H
#define MAP_H

#include <vector>

#include "SDL.h"

typedef struct {
	int w;
	int h;
} Field;

const Field FieldSize[] = { {100,200} , {200,300} };

enum FieldTemplate {
	TOWER_A = 0,
	TOWER_B = 1
};

class Map {
	private:
		int _w, _h;
		int _x_overlap, _y_overlap;
		int _new_x, _new_y;

	public:
		Map(const int w, const int h, const int xlap, const int ylap);
		~Map();

		void addBuilding(SDL_Texture *tx, const FieldTemplate tp);
		void render();
};

#endif /* MAP_H */
