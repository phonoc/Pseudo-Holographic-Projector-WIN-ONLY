#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#include <vector>
#include <unordered_set>

#include "GL/glew.h"
#include "logger.hpp"
#include "model.hpp"

enum {
	SHADOWMAP_WIDTH = 4096,
	SHADOWMAP_HEIGHT = 4096
};

class ShadowMap {
	private:
		Shader * _shadowShader;
		GLuint _depthMapFrameBufferObject;
		GLuint _depthMapTexture;
		Mesh::LightSource * _lightSource;
		std::unordered_set<Mesh::Mesh *> _meshes;
		std::unordered_set<Model *> _models;

	public:
		ShadowMap(Mesh::LightSource * lsrc);
		~ShadowMap();

		void bindLightSource(Mesh::LightSource * src);
		void unbindLightSource();
		void bindMesh(Mesh::Mesh * mesh);
		int unbindMesh(Mesh::Mesh * mesh);
		void bindModel(Model * model);
		int unbindModel(Model * model);
		void render();
};

#endif /* SHADOWMAP_H */
