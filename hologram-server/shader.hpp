#ifndef SHADER_H
#define SHADER_H

#include <cstdlib>
#include <cstdio>
#include <string>

#include "GL/glew.h"

class Shader {
	private:
		GLuint _vertexShader, _fragmentShader;
		GLuint _shaderProgramID;
		GLuint loadShaderProgram(const std::string& vs_path, const std::string& fs_path);

	public: 
		Shader();
		Shader(const std::string& vs_path, const std::string& fs_path);
		~Shader();

		void use();
		GLuint getProgram() const;
};

#endif /* SHADER_H */
