#ifndef CONFIG_H
#define CONFIG_H

#include <cinttypes>

namespace CONFIG {
	
	/* Getters */
	int GET_WIN_WIDTH();
	int GET_WIN_HEIGHT();
	bool GET_IS_WINDOWED();
	bool GET_IS_FPS_CAP();
	double GET_FPS();
	int GET_MAP_WIDTH();
	int GET_MAP_HEIGHT();
	int GET_PORT();
	uint64_t GET_PACKET_SIZE();
	int GET_BUFFER_SIZE();
	int GET_PLANE_WIDTH();

	/* Setters */
	void SET_WIN_WIDTH(const int w);
	void SET_WIN_HEIGHT(const int h);
	void SET_WINDOWED(const bool w);
	void SET_FPS_CAP(const bool c);
	void SET_FPS(const double fps);
	void SET_MAP_WIDTH(const int w);
	void SET_MAP_HEIGHT(const int h);
	void SET_PORT(const unsigned short p);
	void SET_PACKET_SIZE(const uint64_t s);
	void SET_BUFFER_SIZE(const int s);
	void SET_PLANE_WIDTH(const int w);

} /*namespace CONFIG */

#endif /* CONFIG_H */
