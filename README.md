What is this?
=============
The client captures drawing of a tower on a paper template using internal webcam, it then sends the textures to the server to render to multiple monitors to generate holographic effect. The client also sends the textures to the program at the other side of the room (not provided in this repository).

Preparing Solution
=================

Linking
-------
Go to _Project_-> _Properties_ -> _Linker_ *category* -> _System_ *subcategory* and change _SubSystem_ to `Console` or `Windows`

Usage
=====
- Use `W`,`A`,`S`,`D` or arrow keys to move around the scene
- Combining `LSHIFT` with movement keys will enter rotate mode
- Press `L` to toggle lamp visibility