#ifndef CONFIG_H
#define CONFIG_H

#include <cinttypes>
#include <string>

namespace CONFIG {
	
	/* Getters */
	int GET_WIN_WIDTH();
	int GET_WIN_HEIGHT();
	bool GET_IS_WINDOWED();
	bool GET_IS_FPS_CAP();
	double GET_FPS();
	int GET_MAP_WIDTH();
	int GET_MAP_HEIGHT();
	int GET_PORT();
	uint64_t GET_PACKET_SIZE();
	int GET_BUFFER_SIZE();
	int GET_UNDO_LEVELS();
	int GET_MAX_AREA();
	int GET_MIN_AREA();
	std::string GET_SENDING_HOST();
	std::string GET_RENDER_HOST();
	std::string GET_SENDING_USR();
	std::string GET_SENDING_PWD();

	/* Setters */
	void SET_WIN_WIDTH(const int w);
	void SET_WIN_HEIGHT(const int h);
	void SET_WINDOWED(const bool w);
	void SET_FPS_CAP(const bool c);
	void SET_FPS(const double fps);
	void SET_MAP_WIDTH(const int w);
	void SET_MAP_HEIGHT(const int h);
	void SET_PORT(const unsigned short p);
	void SET_PACKET_SIZE(const uint64_t s);
	void SET_BUFFER_SIZE(const int s);
	void SET_UNDO_LEVELS(const int level);
	void SET_MAX_AREA(const int max);
	void SET_MIN_AREA(const int min);
	void SET_SENDING_HOST(const std::string &host);
	void SET_SENDING_USR(const std::string &usr);
	void SET_SENDING_PWD(const std::string &pwd);
	void SET_RENDER_HOST(const std::string &host);

} /*namespace CONFIG */

#endif /* CONFIG_H */
