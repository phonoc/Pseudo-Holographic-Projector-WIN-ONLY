#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"

#include "main.hpp"
#include "config.hpp"
#include "logger.hpp"
#include "interface_controller.hpp"
#include "tracking.hpp"
#include "switch.hpp"

void EnterMainLoop(SDL_Renderer *mainRenderer){

    // Object Initializations
    InterfaceController itfCtrl(mainRenderer);
	Tracking tracker;
	tracker.connectToServer(CONFIG::GET_RENDER_HOST());

    // Main Loop
    const int ms_per_frame = 1000/FPS;
    bool running = true;
    while (running){
        uint32_t time_counter = SDL_GetTicks();

        // Clear drawing buffer
        SDL_SetRenderDrawColor(mainRenderer, 0, 0, 0, 0xff);
        SDL_RenderClear(mainRenderer);
		tracker.updateCameraFeedback();

        // Fetch events
        SDL_Event event;
        while(SDL_PollEvent(&event)){
            const uint8_t *key_state = SDL_GetKeyboardState(nullptr);
            if (key_state[SDL_SCANCODE_ESCAPE]) running = false;

			switch (event.type) {
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
						case SDLK_SPACE:
							tracker.getImage();
							//tracker.SendImagebyFTP();
						break;
					}
				break;
			}

            // Handle Events
            itfCtrl.HandleEvent(&event);
        }

        // Render objects
        itfCtrl.Render();

        // Execute routines which must always be run
        itfCtrl.AlwaysRoutine();

        // Update window
        SDL_RenderPresent(mainRenderer);
		tracker.showCameraFeedback();

        // FPS Capping
        #ifdef FPS_CAP
        time_counter = SDL_GetTicks() - time_counter;
        if (time_counter < ms_per_frame){
            SDL_Delay(ms_per_frame - time_counter);
        }
        #endif
    }

	tracker.disconnectFromServer();
}

void InitRenderer(SDL_Renderer *ren){
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    SDL_RenderSetLogicalSize(ren, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT());
    SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
}

void SystemInit() {
	/* Enable logging */
	logger::setMonOut(true);
	logger::start("hologram-client.log");
	std::atexit(logger::stop);

	/* Misc. */
	std::srand(std::time(nullptr));
	SDL_DisableScreenSaver();
	SDL_ShowCursor(SDL_DISABLE);
}

int main(int argc, char** argv){

	SystemInit();

	Switch options;
	if (!options.parse(argc, argv)) return EXIT_FAILURE;

    if (SDL_Init(SDL_INIT_VIDEO) == 0){
		uint32_t flags = SDL_WINDOW_SHOWN;

		if (!CONFIG::GET_IS_WINDOWED()) flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;

        SDL_Window *mainWindow = SDL_CreateWindow("Pseudo-Holographic Projector", SDL_WINDOWPOS_CENTERED_DISPLAY(0),
               SDL_WINDOWPOS_CENTERED_DISPLAY(0), CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT(), SDL_WINDOW_SHOWN); 
        if (mainWindow != nullptr){
            SDL_Renderer *mainRenderer = SDL_CreateRenderer(mainWindow, -1, SDL_RENDERER_ACCELERATED);
            InitRenderer(mainRenderer);
            if (mainRenderer != nullptr){
                if (TTF_Init() == 0){
                    if (IMG_Init(IMG_INIT_PNG) == IMG_INIT_PNG){
                        EnterMainLoop(mainRenderer);
                        SDL_DestroyRenderer(mainRenderer);
                    }
                    else{
						logger::putError("Failed to initialize SDL_image: " + std::string(IMG_GetError()));
                    }
                    IMG_Quit();
                }
                else{
					logger::putError("Failed to initialize SDL_ttf: " + std::string(TTF_GetError()));
                }
                TTF_Quit();
            }
            else{
				logger::putError("Failed to create renderer: " + std::string(SDL_GetError()));
            }
            SDL_DestroyWindow(mainWindow);
        }
        else{
            logger::putError("Failed to create window: " + std::string(SDL_GetError()));
        }
        SDL_Quit();
    }
    else{
		logger::putError("Failed to initialize SDL: " + std::string(SDL_GetError()));
    }
    
    return EXIT_SUCCESS;
}
