#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <iostream>
#include <string>

namespace logger {

	void start(const std::string& fileName);
	void stop();
	void setMonOut(const bool out);

	bool putError(const std::string& msg);
	void putInfo(const std::string& msg);
	void putWarning(const std::string& msg);

} /* namespace logging */

#endif /* LOGGER_H */
