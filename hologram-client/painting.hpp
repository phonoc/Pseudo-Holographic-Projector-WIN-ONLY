#ifndef PAINTING_H
#define PAINTING_H

#include "SDL.h"

namespace painting{

class BufferHistory{

private:
    SDL_Surface **_history;
    SDL_Texture *_tx_stream;
    int _focus;
    int _latest;
    void UpdateTexture();

public:
    BufferHistory();
    ~BufferHistory();
    
    void Draw(SDL_Surface *surface);
    void Undo();
    void Redo();
    
    SDL_Surface *GetCurrentSurface();
    SDL_Texture *GetStreamingTexture();
};

BufferHistory *GetDrawHistory();

void Init(SDL_Renderer *ren);
void CleanUp();

class LinePainter{

private:
    bool _drawing;
    int _start_mouse_x, _start_mouse_y;
    void DrawLine(SDL_Surface *sf, const int x1, 
            const int y1, const int x2, const int y2);
    void OnClick(SDL_Event *event);

public:
    LinePainter();
    
    void HandleEvent(SDL_Event *event);
    void AlwaysRoutine();
    bool IsDrawing() const;
};

class AreaPainter{

private:
    bool **_visited;
    bool IsInBound(const int x, const int y,
            uint32_t root_clr, uint32_t *pixels);
    void OnClick(SDL_Event *event);

public:
    AreaPainter();
    ~AreaPainter();

    void HandleEvent(SDL_Event *event);
    void AlwaysRoutine();
};

} /* namespace painting */

#endif /* PAINTING_H */
