#include <ctime>

#include "logger.hpp"

namespace logger {

	std::ofstream _logStream;
	std::string _logFile;
	bool _isMonOut = false;

	std::string getCurrentTimeString() {
		time_t timer = time(nullptr);
		struct tm timeInfo;
		localtime_s(&timeInfo, &timer);
		char tmp[31];
		strftime(tmp, 31, "%T", &timeInfo);
		return std::string(tmp);
	}

	bool putError(const std::string& msg) {
		_logStream << "(" << getCurrentTimeString() << ") [ERROR] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [ERROR] " + msg << std::endl;
		return false;
	}

	void putWarning(const std::string& msg) {
		_logStream << "(" << getCurrentTimeString() << ") [WARNING] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [WARNING] " + msg << std::endl;
	}

	void putInfo(const std::string& msg) {
		_logStream << "(" << getCurrentTimeString() << ") [INFO] " + msg << std::endl;
		if (_isMonOut) std::cerr << "(" << getCurrentTimeString() << ") [INFO] " + msg << std::endl;
	}

	void start(const std::string& fileName) {
		_logFile = fileName;
		_logStream.open(fileName.c_str(), _logStream.out);
	}

	void stop() {
		_logStream.close();
	}

	void setMonOut(const bool out) {
		_isMonOut = out;
	}

} /* namespace logger */