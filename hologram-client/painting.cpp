#include "painting.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cmath>
#include <queue>

#include "config.hpp"
#include "logger.hpp"

namespace painting{

static BufferHistory *DrawHistory;
static SDL_Renderer *MainRenderer;

void Init(SDL_Renderer *ren){
    MainRenderer = ren;
    DrawHistory = new BufferHistory();
}

void CleanUp(){
    delete DrawHistory;
}

int inline Xy2PixelIndex(const int x, const int y){
    return (y * CONFIG::GET_WIN_WIDTH() + x);
}

////
//      BufferHistory Implementation
////
BufferHistory *GetDrawHistory(){
    return DrawHistory;
}

BufferHistory::BufferHistory(){
    _history = (SDL_Surface **)(std::calloc(CONFIG::GET_UNDO_LEVELS(), sizeof(SDL_Surface*)));
    _focus = -1;
    _latest = -1;
    _tx_stream = SDL_CreateTexture(MainRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING,
            CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT());

    SDL_Surface *bed_surface = SDL_CreateRGBSurface(0, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT(), 32, 0, 0, 0, 0);
    SDL_FillRect(bed_surface, nullptr, SDL_MapRGBA(bed_surface->format, 0, 0, 0, 0xff));
    Draw(bed_surface);
}

BufferHistory::~BufferHistory(){
    for (int i=0; i <= _latest; ++i){
        if (_history[i] != nullptr) SDL_FreeSurface(_history[i]);
    }
    std::free(_history);
    SDL_DestroyTexture(_tx_stream);
}

void BufferHistory::UpdateTexture(){
    SDL_Surface *csf = GetCurrentSurface();
    if (csf != nullptr) SDL_UpdateTexture(_tx_stream, nullptr, csf->pixels, csf->pitch);
}

void BufferHistory::Undo(){
    if (_focus > 0){
        --_focus;
    }
    UpdateTexture();
}

void BufferHistory::Redo(){
    if (_focus < _latest){
        ++_focus;
    }
    UpdateTexture();
}

void BufferHistory::Draw(SDL_Surface *surface){
    int to_replace = _focus + 1;
    if (to_replace >= CONFIG::GET_UNDO_LEVELS()){
        to_replace = 1;
    }
    if (_history[to_replace] != nullptr){
        SDL_FreeSurface(_history[to_replace]);
    }
    _history[to_replace] = surface;
    _focus = to_replace;
    _latest = _focus;
    UpdateTexture();
}

SDL_Surface *BufferHistory::GetCurrentSurface(){
    if (_focus < 1) return nullptr;
    return _history[_focus];
}

SDL_Texture *BufferHistory::GetStreamingTexture(){
    if (_focus < 1) return nullptr;
    return _tx_stream;
}

////
//      LinePainter Implementation
////
LinePainter::LinePainter(){
    _drawing = false;
}

void LinePainter::HandleEvent(SDL_Event *event){
    switch(event->type){
        case SDL_MOUSEBUTTONDOWN:
            OnClick(event);
        break;
    }
}

void LinePainter::AlwaysRoutine(){ 
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    if (_drawing){
        // Preview line
        SDL_SetRenderDrawColor(MainRenderer, 0xff, 0, 0, 0xff);
        SDL_RenderDrawLine(MainRenderer, _start_mouse_x, _start_mouse_y, mouse_x, mouse_y);
    }
}

void LinePainter::OnClick(SDL_Event *event){
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);

    if (event->button.button == SDL_BUTTON_LEFT){
        if (_drawing){
            SDL_Surface *new_surface = SDL_CreateRGBSurface(0, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT(), 32, 0, 0, 0, 0);
            SDL_Surface *current_surface = DrawHistory->GetCurrentSurface();
            if (current_surface != nullptr){
                SDL_BlitSurface(current_surface, nullptr, new_surface, nullptr);
            } 
            DrawLine(new_surface, _start_mouse_x, _start_mouse_y, mouse_x, mouse_y);
            DrawHistory->Draw(new_surface);
        }
        else{
            _start_mouse_x = mouse_x;
            _start_mouse_y = mouse_y;
        }
        _drawing = !_drawing;
    }
}

void LinePainter::DrawLine(SDL_Surface *sf, const int x1, const int y1, const int x2, const int y2){
    SDL_LockSurface(sf);
    uint32_t *pixels = (uint32_t *) sf->pixels;
    double angle = std::atan2(y2-y1, x2-x1);
    double sine = std::sin(angle), cosine = std::cos(angle);
    int dist = (int) std::sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    uint32_t color = SDL_MapRGBA(sf->format, std::rand()%256, std::rand()%256, std::rand()%256, 0xff);
    //uint32_t color = SDL_MapRGBA(sf->format, 255, 0, 0, 255);
    for (int i=0; i<dist; ++i){
        pixels[Xy2PixelIndex((int) x1+i*cosine, (int) y1+i*sine)] = color;
    }
    SDL_UnlockSurface(sf);
}

bool LinePainter::IsDrawing() const{
    return _drawing;
}

////
//      AreaPainter Implementation
////
AreaPainter::AreaPainter(){
    _visited = (bool **) std::malloc(sizeof(bool *) * CONFIG::GET_WIN_WIDTH());
    for (int i=0; i<CONFIG::GET_WIN_WIDTH(); ++i){
        _visited[i] = (bool *) std::malloc(sizeof(bool) * CONFIG::GET_WIN_HEIGHT());
    }
}

AreaPainter::~AreaPainter(){
    for (int i=0; i<CONFIG::GET_WIN_WIDTH(); ++i){
        std::free(_visited[i]);
    }
    std::free(_visited);
}

bool AreaPainter::IsInBound(const int x, const int y, uint32_t root_color, uint32_t *pixels){
    if (x < 0 || y < 0 || x >= CONFIG::GET_WIN_WIDTH() || y >= CONFIG::GET_WIN_HEIGHT()) return false;
    if (_visited[x][y]) return false;
    if (root_color != pixels[Xy2PixelIndex(x, y)]) return false;
    return true;
}

void AreaPainter::OnClick(SDL_Event *event){
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    SDL_Point root_point;
    root_point.x = mouse_x;
    root_point.y = mouse_y;

    uint32_t *pixels = nullptr;
    SDL_Surface *new_surface = SDL_CreateRGBSurface(0, CONFIG::GET_WIN_WIDTH(), CONFIG::GET_WIN_HEIGHT(), 32, 0, 0, 0, 0);
    SDL_Surface *current_surface = DrawHistory->GetCurrentSurface();
    SDL_BlitSurface(current_surface, nullptr, new_surface, nullptr);
    SDL_LockSurface(new_surface);
    pixels = (uint32_t *) new_surface->pixels;

    uint32_t root_color = pixels[Xy2PixelIndex(mouse_x, mouse_y)];
    for (int i=0; i<CONFIG::GET_WIN_WIDTH(); ++i){
        for (int j=0; j<CONFIG::GET_WIN_HEIGHT(); ++j){
            _visited[i][j] = false;
        }
    }

    std::queue<SDL_Point> *bfs_q = new std::queue<SDL_Point>;
    bfs_q->push(root_point);
    _visited[root_point.x][root_point.y] = true;
    uint32_t new_color = SDL_MapRGBA(new_surface->format, std::rand()%256, std::rand()%256, std::rand()%256, 0xff);
    while (bfs_q->size() > 0){
        // Visit node
        int x, y;
        x = bfs_q->front().x;
        y = bfs_q->front().y;

        bfs_q->pop();
        pixels[Xy2PixelIndex(x, y)] = new_color;

        // Enqueue adjacent nodes
        SDL_Point tp;
        if (IsInBound(x, y-1, root_color, pixels)){
            tp.x = x; tp.y = y-1;
            bfs_q->push(tp);
            _visited[tp.x][tp.y] = true;
        }
        if (IsInBound(x-1, y, root_color, pixels)){
            tp.x = x-1; tp.y = y;
            bfs_q->push(tp);
            _visited[tp.x][tp.y] = true;
        }
        if (IsInBound(x+1, y, root_color, pixels)){
            tp.x = x+1; tp.y = y;
            bfs_q->push(tp);
            _visited[tp.x][tp.y] = true;
        }
        if (IsInBound(x, y+1, root_color, pixels)){
            tp.x = x; tp.y = y+1;
            bfs_q->push(tp);
            _visited[tp.x][tp.y] = true;
        }
    }
    delete bfs_q;
    SDL_UnlockSurface(new_surface);
    DrawHistory->Draw(new_surface);
}

void AreaPainter::HandleEvent(SDL_Event *event){
    switch(event->type){
        case SDL_MOUSEBUTTONDOWN:
            if (event->button.button == SDL_BUTTON_LEFT) OnClick(event);
        break;
    }
}

void AreaPainter::AlwaysRoutine(){
    // TODO
}

} /* namespace painting */
