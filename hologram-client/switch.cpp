#include <cstring>
#include <vector>

#include "switch.hpp"
#include "hard_config.hpp"
#include "config.hpp"

bool Switch::parse(int argc, char **argv) {

	std::vector<bool> isParam(argc);
	for (int i = 1; i < argc; ++i) {

		// check if it's an option
		if (argv[i][0] == '-') {
			size_t len = std::strlen(argv[i]);
			if (len > 1) {
				// check if it's a long option
				if (argv[i][1] == '-') {
					if (len > 2) {
						if (!std::strcmp(argv[i] + 2, "windowed")) {
							// --windowed
							CONFIG::SET_WINDOWED(true);
						}
						else if (!std::strcmp(argv[i] + 2, "width")) {
							// --width <w>
							if (i == argc - 1) {
								return logger::putError("Please specify windows' width.");
							}
							else {
								int w = std::atoi(argv[i + 1]);
								if (!w) {
									return logger::putError("Invalid width");
								}
								CONFIG::SET_WIN_WIDTH(w);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "height")) {
							// --height <h>
							if (i == argc - 1) {
								return logger::putError("Please specify windows' height.");
							}
							else {
								int h = std::atoi(argv[i + 1]);
								if (!h) {
									return logger::putError("Invalid height");
								}
								CONFIG::SET_WIN_HEIGHT(h);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "map-width")) {
							// --map-width <w>
							if (i == argc - 1) {
								return logger::putError("Please specify map's width.");
							}
							else {
								int w = std::atoi(argv[i + 1]);
								if (!w) {
									return logger::putError("Invalid width");
								}
								CONFIG::SET_MAP_WIDTH(w);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "map-height")) {
							// --map-height <h>
							if (i == argc - 1) {
								return logger::putError("Please specify map's height.");
							}
							else {
								int h = std::atoi(argv[i + 1]);
								if (!h) {
									return logger::putError("Invalid height");
								}
								CONFIG::SET_MAP_WIDTH(h);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "port")) {
							// --port <p>
							if (i == argc - 1) {
								return logger::putError("Please specify port number (0-65535).");
							}
							else {
								int p = std::atoi(argv[i + 1]);
								if (p < 0 || p > 0xffff) {
									return logger::putError("Invalid port");
								}
								CONFIG::SET_PORT((unsigned short) p);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "packet-size")) {
							// --packet-size <s>
							if (i == argc - 1) {
								return logger::putError("Please specify packet size.");
							}
							else {
								uint64_t s = std::strtoul(argv[i + 1], nullptr, 0);
								if (s == 0) {
									return logger::putError("Invalid packet size");
								}
								CONFIG::SET_PACKET_SIZE(s);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "buffer-size")) {
							// --buffer-size <s>
							if (i == argc - 1) {
								return logger::putError("Please specify buffer size.");
							}
							else {
								int s = std::atoi(argv[i + 1]);
								if (s <= 0) {
									return logger::putError("Invalid packet size");
								}
								CONFIG::SET_BUFFER_SIZE(s);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "cap-fps")) {
							// --cap-fps
							CONFIG::SET_FPS_CAP(true);
						}
						else if (!std::strcmp(argv[i] + 2, "fps")) {
							// --fps <fps>
							if (i == argc - 1) {
								return logger::putError("Please specify custom FPS.");
							}
							else {
								double fps;
								try {
									fps = std::stod(std::string(argv[i + 1]));
								}
								catch(std::invalid_argument e){
									return logger::putError("Invalid FPS");
								}
								CONFIG::SET_FPS(fps);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "max-area")) {
							// --max-area <area>
							if (i == argc - 1) {
								return logger::putError("Please specify max area.");
							}
							else {
								int maxarea;
								maxarea = std::atoi(argv[i + 1]);
								CONFIG::SET_MAX_AREA(maxarea);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "min-area")) {
							// --min-area <area>
							if (i == argc - 1) {
								return logger::putError("Please specify min area.");
							}
							else {
								int minarea;
								minarea = std::atoi(argv[i + 1]);
								CONFIG::SET_MAX_AREA(minarea);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "ftp-host")) {
							// --host <string>
							if (i == argc - 1) {
								return logger::putError("Please specify host.");
							}
							else {
								std::string host;
								host = argv[i + 1];
								CONFIG::SET_SENDING_HOST(host);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "render-host")) {
							// --render-host <string>
							if (i == argc - 1) {
								return logger::putError("Please specify render host.");
							}
							else {
								std::string host;
								host = argv[i + 1];
								CONFIG::SET_RENDER_HOST(host);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "user")) {
							// --user <string>
							if (i == argc - 1) {
								return logger::putError("Please specify user.");
							}
							else {
								std::string user;
								user = argv[i + 1];
								CONFIG::SET_SENDING_USR(user);
								isParam[i + 1] = true;
							}
						}
						else if (!std::strcmp(argv[i] + 2, "passwd")) {
							// --passwd <string>
							if (i == argc - 1) {
								return logger::putError("Please specify password.");
							}
							else {
								std::string passwd;
								passwd = argv[i + 1];
								CONFIG::SET_SENDING_PWD(passwd);
								isParam[i + 1] = true;
							}
						}
						else {
							return logger::putError("Unknown switch: '" + std::string(argv[i] + 2) + "'");
						}
					}
					else {
						return logger::putError("Unknown switch");
					}
				}
				else {
					// is short option
					for (char *opt = argv[i] + 1; *opt != 0; ++opt) {
						switch (*opt) {
							case 'x':
								CONFIG::SET_WINDOWED(true);
							break;
							case 'c':
								CONFIG::SET_FPS_CAP(true);
							break;
							case 'f':
								CONFIG::SET_WINDOWED(false);
							break;
							default: return logger::putError("Unknown switch: '" + std::string(opt) + "'");
						}
					}
				}
			}
			else {
				return logger::putError("Unknown switch");
			}
		}
		else if (!isParam[i]) {
			return logger::putError("Invalid switch: '" + std::string(argv[i]) + "'");
		}
	}

	return true;
}
