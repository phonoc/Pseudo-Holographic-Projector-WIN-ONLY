#ifndef INTERFACE_CONTROLLER_H
#define INTERFACE_CONTROLLER_H

#include <cstdio>

#include "SDL.h"
#include "SDL_ttf.h"

#include "config.hpp"
#include "painting.hpp"

enum{
    MODE_DRAW,
    MODE_FILL
};

class InterfaceController{

private:
    SDL_Renderer *_renderer;
    SDL_Texture *_tx_label_draw,
                *_tx_label_fill;
    TTF_Font *_font;
    int _mode;
    bool _visible;
    bool _key_locked;
    painting::LinePainter _line_painter;
    painting::AreaPainter _area_painter;
    painting::BufferHistory *_draw_history;

    void OnClick(SDL_Event *event);
    void OnKey(SDL_Event *event);

public:
    InterfaceController(SDL_Renderer *ren);
    ~InterfaceController();
    void HandleEvent(SDL_Event *event);
    void AlwaysRoutine();
    void Render();
};

#endif /* INTERFACE_CONTROLLER_H */
