#ifndef TRACKING_H
#define TRACKING_H
#define _WINSOCKAPI_
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv/cv.h"
#include "opencv2/imgproc.hpp"
#include "SDL.h"
#include <string>
/*#include "curl\curl.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <io.h>*/
#include <wininet.h> 
#include <ctime>

#pragma comment(lib, "Wininet")
#pragma comment(lib, "wsock32.lib")

class Tracking {
	private:
		WSADATA wsaData;
		WORD version;
		int error;
		SOCKET client;
		struct sockaddr_in server;
		char *message;
		IplImage *sendd;
		SDL_Surface *surface;
		cv::String _feedWindowName;
		cv::VideoCapture * _camera;
		cv::Mat _cameraFrame;

	public:
		Tracking();
		~Tracking();
		void updateCameraFeedback();
		void showCameraFeedback();
		bool connectToServer(const std::string& address);
		bool disconnectFromServer();
		int getImage();
		bool sendSDLSurfaceToServer(int type, cv::Mat *img);
		bool senduint32(std::string what, uint32_t n);
		bool SendImagebyFTP(int type);
};

#endif 