#include <vector>

#include "tracking.hpp"
#include "logger.hpp"
#include "config.hpp"
#include "SDL_image.h"

Tracking::Tracking() {
	_feedWindowName = "Camera Feedback";
	cv::namedWindow(_feedWindowName, CV_WINDOW_AUTOSIZE);
	_camera = new cv::VideoCapture(0);
	if (!_camera->isOpened()) {
		logger::putError("Unable to open camera");
		return;
	}
}

Tracking::~Tracking() {
	_camera->release();
}

void Tracking::updateCameraFeedback() {
	_camera->read(_cameraFrame);
}

void Tracking::showCameraFeedback() {
	imshow(_feedWindowName, _cameraFrame);
}

bool Tracking::connectToServer(const std::string& address) {

	// Create socket
	version = MAKEWORD(2, 0);
	error = WSAStartup(version, &wsaData);
	if (error != 0) {
		WSACleanup();
		return EXIT_FAILURE;
	}
	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 0) {
		WSACleanup();
		return EXIT_FAILURE;
	}
	if ((client = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		logger::putError("Could not create socket: code " + std::to_string(WSAGetLastError()));
		WSACleanup();
		return EXIT_FAILURE;
	}

	// Connect to server
	server.sin_addr.s_addr = inet_addr(address.c_str());
	server.sin_family = AF_INET;
	server.sin_port = htons(55555);
	if (connect(client, (struct sockaddr *)&server, sizeof(server)) != 0) {
		return logger::putError("Could not find server");
	}

	logger::putInfo("Connected successfully");
	return EXIT_SUCCESS;
}

bool Tracking::senduint32(std::string what, uint32_t data){
	char *buf = new char[sizeof(uint32_t)];
	char *p = buf;
	memcpy(buf, &data, sizeof(uint32_t));
	size_t length = sizeof(uint32_t);
	size_t n = 0;
	while (length > 0)
	{
		n = send(client, p, length, 0);
		if (n <= 0) break;
		p += n;
		length -= n;
	}
	if (n <= 0) {
		return logger::putError("Unable to send " + what);
	}
	delete(buf);
	return EXIT_SUCCESS;
}

uint32_t loadFile(const std::string& path, std::vector<char> * container) {
	std::ifstream file;
	file.open(path.c_str(), std::ifstream::binary);

	uint32_t size = 0;
	while (!file.eof()) {
		container->push_back(file.get());
		++size;
	}
	logger::putInfo("size = " + std::to_string(size));
	file.close();
	return size;
}

bool Tracking::sendSDLSurfaceToServer(int type, cv::Mat *img) {

	std::vector<char> pngData;
	uint32_t image_size = loadFile("temp.png", &pngData);

	// send type of image
	if (senduint32("type", type)) {
		return EXIT_FAILURE;
	}

	// send size of image
	//if (senduint32("size", img->total()*img->elemSize())) {
	//if (senduint32("size", image_size)){
	if (senduint32("size", image_size)) {
		return EXIT_FAILURE;
	}

	// send width of image
	//if (senduint32("width", img->cols)) {
	if (senduint32("width", img->cols)) {
		return EXIT_FAILURE;
	}

	// send height of image
	//if (senduint32("height", img->rows)) {
	if (senduint32("height", img->rows)) {
		return EXIT_FAILURE;
	}
	/*cvNamedWindow("image", CV_WINDOW_AUTOSIZE);
	cvShowImage("image", img);*/
	// send imagedata
	logger::putInfo("Allocating " + std::to_string(image_size) + " bytes of image data...");
	char *buf = new char[image_size];
	//char *buf = new char[eiei->pitch * eiei->h];
	char *p = buf;
	//char *p = (char *) eiei->pixels;
	memcpy(buf, &pngData[0], image_size);
	//memcpy(buf, eiei->pixels, eiei->pitch * eiei->h);
	size_t length = image_size;
	//size_t length = eiei->pitch * eiei->h;
	size_t n = 1;
	while (length > 0)
	{
		n = send(client, p, length, 0);
		if (n <= 0) break;
		p += n;
		length -= n;
	}
	if (n <= 0) {
		return logger::putError("Unable to send image data");
	}
	delete(buf);

	logger::putInfo("All data has been successfully sent.");
	return EXIT_SUCCESS;
}

bool Tracking::disconnectFromServer()
{
	if (shutdown(client, SD_SEND) == SOCKET_ERROR) {
		logger::putError("Shutdown error: " + std::to_string(WSAGetLastError()));
		closesocket(client);
		WSACleanup();
		return EXIT_FAILURE;
	}
	WSACleanup();
	return EXIT_SUCCESS;
}

int Tracking::getImage()
{
	int type = -1;
	//cv::Mat src = cv::imread("D:/a.jpg");
	
	cv::Mat src;
	_camera->read(src);

	logger::putInfo("Image is captured successfully.");
	cv::Mat thr(src.rows, src.cols, CV_8UC1);
	cv::cvtColor(src, thr, CV_BGR2GRAY);
	//cv::threshold(thr, thr, 128, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);
	cv::adaptiveThreshold(thr, thr, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 11, 6);

	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(thr, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	double a = 0;
	bool distanceOk = false, shapeOk = false;
	for (int i = 0; i < contours.size(); i++)
	{
		a = cv::contourArea(contours[i], false);
		if (a > 10000) logger::putInfo("Area = " + std::to_string(a));
		if (a<CONFIG::GET_MAX_AREA() && a>CONFIG::GET_MIN_AREA())
		{
			distanceOk = true;
			std::vector<std::vector<cv::Point>> contours_poly(1);
			approxPolyDP(cv::Mat(contours[i]), contours_poly[0], cv::arcLength(cv::Mat(contours[i]), true)*0.07, true);
			//approxPolyDP(contours[i], approxrec, arcLength(Mat(contours[i]), true)*0.02, true);
			cv::Rect boundRect = cv::boundingRect(contours[i]);
			logger::putInfo("poly = " + std::to_string(contours_poly[0].size()));
			if (contours_poly[0].size() == 4)
			{
				std::vector<cv::Point2f> quad_pts;
				std::vector<cv::Point2f> squre_pts;
				quad_pts.push_back(cv::Point2f(contours_poly[0][0].x, contours_poly[0][0].y));
				quad_pts.push_back(cv::Point2f(contours_poly[0][1].x, contours_poly[0][1].y));
				quad_pts.push_back(cv::Point2f(contours_poly[0][3].x, contours_poly[0][3].y));
				quad_pts.push_back(cv::Point2f(contours_poly[0][2].x, contours_poly[0][2].y));
				squre_pts.push_back(cv::Point2f(boundRect.x, boundRect.y));
				squre_pts.push_back(cv::Point2f(boundRect.x, boundRect.y + boundRect.height));
				squre_pts.push_back(cv::Point2f(boundRect.x + boundRect.width, boundRect.y));
				squre_pts.push_back(cv::Point2f(boundRect.x + boundRect.width, boundRect.y + boundRect.height));
				cv::Mat transmtx = getPerspectiveTransform(quad_pts, squre_pts);
				cv::Mat transformed = cv::Mat::zeros(src.rows, src.cols, CV_8UC3);
				warpPerspective(src, transformed, transmtx, src.size());

				cv::Mat crop = transformed(boundRect);
				int t = 0;
				for (int j = 2; j >= 1; --j)
				{
					t *= 2;
					cv::Vec3b color = crop.at<cv::Vec3b>(j*crop.size().height/ 3, crop.size().width / 35);
					if (color.val[0] <= 75) {
						t += 1;
					}
				}
				shapeOk = true;
				type = t;
				if (type < 1) {
					logger::putInfo("Couldn't determine building type");
					continue;
				}
				logger::putInfo("Detected building type " + std::to_string(type));
				cv::imwrite("temp.png", crop);
				if (sendSDLSurfaceToServer(type, &crop) == EXIT_FAILURE) {
					logger::putError("Failed to send raw pixels to rendering server");
				}
				if (SendImagebyFTP(type))
					logger::putInfo("sent by FTP successfully");
				else
					logger::putError("Failed to send by FTP");
			}
			break;
		}
	}
	if (!distanceOk) {
		logger::putError("Layout misaligned. (Template is too far or too close.)");
	}
	else {
		if (!shapeOk) {
			logger::putError("Layout misaligned. (No rectangle has been detected.)");
		}
	}
	return type;
}

bool Tracking::SendImagebyFTP(int type)
{
	HINTERNET hInternet;
	HINTERNET hFtpSession;
	hInternet = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
	if (hInternet == NULL)
	{
		return false;
	}
	else
	{
		hFtpSession = InternetConnect(hInternet, const_cast<char *>(CONFIG::GET_SENDING_HOST().c_str()), 21, const_cast<char *>(CONFIG::GET_SENDING_USR().c_str()), const_cast<char *>(CONFIG::GET_SENDING_PWD().c_str()), INTERNET_SERVICE_FTP, 0, 0);
		if (hFtpSession == NULL)
		{
			return false;
		}
		else
		{
			std::string form = "";
			if (type == 1)
				form = "/b00_";
			else if (type == 2)
				form = "/b01_";
			else
				form = "/b02_";

			time_t rawtime;
			struct tm * timeinfo = new tm();
			char buffer[80];
			time(&rawtime);
			localtime_s(timeinfo,&rawtime);
			strftime(buffer, sizeof(buffer), "%d%H%M%S", timeinfo);
			std::string str(buffer);

			if (!FtpPutFile(hFtpSession, "temp.png", const_cast<char *>((form+str+".png").c_str()), FTP_TRANSFER_TYPE_BINARY, 0))
			{
				return false;
			}
		}
	}
	return true;
}