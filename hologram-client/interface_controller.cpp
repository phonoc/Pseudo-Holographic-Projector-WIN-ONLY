#include <cstdlib>
#include "interface_controller.hpp"
#include "logger.hpp"

InterfaceController::InterfaceController(SDL_Renderer *ren){
    _renderer = ren;
    _mode = MODE_DRAW;
    _visible = false;
    _key_locked = false;

    _font = TTF_OpenFont("terminus.ttf", 14);
    if (_font == nullptr){
		logger::putError("Unable to load TTF font: " + std::string(TTF_GetError()));
		std::exit(EXIT_FAILURE);
		return;
    }
    TTF_SetFontStyle(_font, TTF_STYLE_BOLD);

    SDL_Surface *_sf_label_draw = TTF_RenderText_Blended(_font, "DRAW", {0, 0, 0, 0xFF});
    _tx_label_draw = SDL_CreateTextureFromSurface(_renderer, _sf_label_draw);
    SDL_FreeSurface(_sf_label_draw);

    SDL_Surface *_sf_label_fill = TTF_RenderText_Blended(_font, "FILL", {0, 0, 0, 0xFF});
    _tx_label_fill = SDL_CreateTextureFromSurface(_renderer, _sf_label_fill);
    SDL_FreeSurface(_sf_label_fill);

    painting::Init(_renderer);
    _draw_history = painting::GetDrawHistory();
}   

InterfaceController::~InterfaceController(){
    TTF_CloseFont(_font);
    SDL_DestroyTexture(_tx_label_draw);
    SDL_DestroyTexture(_tx_label_fill);
    painting::CleanUp();
}

void InterfaceController::HandleEvent(SDL_Event *event){
    if (_line_painter.IsDrawing()) _key_locked = true;
    else _key_locked = false;

    if (event->type == SDL_KEYDOWN){
        OnKey(event);
    }
    if (event->type == SDL_MOUSEBUTTONDOWN){
        OnClick(event);
    }
    if (_mode == MODE_DRAW){
        if (_visible) _line_painter.HandleEvent(event);
    }
    else if (_mode == MODE_FILL){
        if (_visible) _area_painter.HandleEvent(event);
    }
}

void InterfaceController::AlwaysRoutine(){
    _line_painter.AlwaysRoutine();
    _area_painter.AlwaysRoutine();
}

void InterfaceController::OnClick(SDL_Event *event){
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
}

void InterfaceController::OnKey(SDL_Event *event){ 
    switch(event->key.keysym.sym){
        case SDLK_h:
            _visible = !_visible;
        break;
        case SDLK_f:
            // switch to filling mode
            if (_visible) if (!_key_locked) _mode = MODE_FILL;
        break;
        case SDLK_d:
            // switch to line drawing mode
            if (_visible) if (!_key_locked) _mode = MODE_DRAW;
        break;
        case SDLK_u:
            // undo a change
            if (!_key_locked) _draw_history->Undo();
        break;
        case SDLK_r:
            // redo a change
            if (!_key_locked) _draw_history->Redo();
        break;
    }
}

void InterfaceController::Render(){
    
    // Paint textures first
    SDL_Texture *current_texture = _draw_history->GetStreamingTexture();
    if (current_texture != nullptr){
        SDL_RenderCopy(_renderer, current_texture, nullptr, nullptr);
    }

    // If interface is hidden, do not paint the interface.
    if (!_visible){
        return;
    }

    // The interface is shown. Draw the boxes.
    const int ypos = CONFIG::GET_WIN_HEIGHT() - 20;
    SDL_Rect rectDraw = {0, ypos, 50, 20};
    if (_mode == MODE_DRAW){
        SDL_SetRenderDrawColor(_renderer, 0xFF, 0xFF, 153, 0xFF);
    }
    else{
        SDL_SetRenderDrawColor(_renderer, 127, 127, 127, 0xFF);
    }
    SDL_RenderFillRect(_renderer, &rectDraw);
    
    if (_mode == MODE_FILL){
        SDL_SetRenderDrawColor(_renderer, 0xFF, 50, 50, 0xFF);
    }
    else{
        SDL_SetRenderDrawColor(_renderer, 127, 127, 127, 0xFF);
    }
    rectDraw = {100, ypos, 50, 20};
    SDL_RenderFillRect(_renderer, &rectDraw);

    // Then label them with text.
    int text_w, text_h;
    SDL_QueryTexture(_tx_label_draw, nullptr, nullptr, &text_w, &text_h);
    rectDraw = {7, ypos+3, text_w, text_h};
    SDL_RenderCopy(_renderer, _tx_label_draw, nullptr, &rectDraw);

    SDL_QueryTexture(_tx_label_fill, nullptr, nullptr, &text_w, &text_h);
    rectDraw = {108, ypos+3, text_w, text_h};
    SDL_RenderCopy(_renderer, _tx_label_fill, nullptr, &rectDraw);
}
