#include "config.hpp"
#include "hard_config.hpp"

namespace CONFIG_VALUES {

	int WIN_WIDTH = HARDCONFIG_WIN_WIDTH;
	int WIN_HEIGHT = HARDCONFIG_WIN_HEIGHT;
	bool IS_WINDOWED = HARDCONFIG_WINDOWED;
	bool IS_FPS_CAP = HARDCONFIG_FPS_CAP;
	double FPS = HARDCONFIG_FPS;
	int MAP_WIDTH = HARDCONFIG_MAP_WIDTH;
	int MAP_HEIGHT = HARDCONFIG_MAP_HEIGHT;
	int PORT = HARDCONFIG_PORT;
	uint64_t PACKET_SIZE = HARDCONFIG_PACKET_SIZE;
	int BUFFER_SIZE = HARDCONFIG_BUFFER_SIZE;
	int UNDO_LEVELS = HARDCONFIG_UNDO_LEVELS;
	int MAX_AREA = HARDCONFIG_MAX_AREA;
	int MIN_AREA = HARDCONFIG_MIN_AREA;
	std::string SENDING_HOST = HARDCONFIG_SENDING_HOST;
	std::string SENDING_USR = HARDCONFIG_SENDING_USR;
	std::string SENDING_PWD = HARDCONFIG_SENDING_PWD;
	std::string RENDER_HOST = HARDCONFIG_RENDER_HOST;

} /* namespace CONFIG_VALUES */

namespace CONFIG {
	int GET_WIN_WIDTH()
	{
		return CONFIG_VALUES::WIN_WIDTH;
	}
	int GET_WIN_HEIGHT()
	{
		return CONFIG_VALUES::WIN_HEIGHT;
	}
	bool GET_IS_WINDOWED()
	{
		return CONFIG_VALUES::IS_WINDOWED;
	}
	bool GET_IS_FPS_CAP()
	{
		return CONFIG_VALUES::IS_FPS_CAP;
	}
	double GET_FPS()
	{
		return CONFIG_VALUES::FPS;
	}
	int GET_MAP_WIDTH()
	{
		return CONFIG_VALUES::MAP_WIDTH;
	}
	int GET_MAP_HEIGHT()
	{
		return CONFIG_VALUES::MAP_HEIGHT;
	}
	int GET_PORT()
	{
		return CONFIG_VALUES::PORT;
	}
	uint64_t GET_PACKET_SIZE()
	{
		return CONFIG_VALUES::PACKET_SIZE;
	}
	int GET_BUFFER_SIZE()
	{
		return CONFIG_VALUES::BUFFER_SIZE;
	}
	int GET_UNDO_LEVELS() {
		return CONFIG_VALUES::UNDO_LEVELS;
	}
	int GET_MAX_AREA() {
		return CONFIG_VALUES::MAX_AREA;
	}
	int GET_MIN_AREA() {
		return CONFIG_VALUES::MIN_AREA;
	}
	std::string GET_SENDING_HOST() {
		return CONFIG_VALUES::SENDING_HOST;
	}
	std::string GET_RENDER_HOST() {
		return CONFIG_VALUES::RENDER_HOST;
	}
	std::string GET_SENDING_USR() {
		return CONFIG_VALUES::SENDING_USR;
	}
	std::string GET_SENDING_PWD() {
		return CONFIG_VALUES::SENDING_PWD;
	}
	void SET_WIN_WIDTH(const int w)
	{
		CONFIG_VALUES::WIN_WIDTH = w;
	}
	void SET_WIN_HEIGHT(const int h)
	{
		CONFIG_VALUES::WIN_HEIGHT = h;
	}
	void SET_WINDOWED(const bool w)
	{
		CONFIG_VALUES::IS_WINDOWED = w;
	}
	void SET_FPS_CAP(const bool c)
	{
		CONFIG_VALUES::IS_FPS_CAP = c;
	}
	void SET_FPS(const double fps)
	{
		CONFIG_VALUES::FPS = fps;
	}

	void SET_MAP_WIDTH(const int w)
	{
		CONFIG_VALUES::MAP_WIDTH = w;
	}

	void SET_MAP_HEIGHT(const int h)
	{
		CONFIG_VALUES::MAP_HEIGHT = h;
	}

	void SET_PORT(const unsigned short p)
	{
		CONFIG_VALUES::PORT = p;
	}

	void SET_PACKET_SIZE(const uint64_t s)
	{
		CONFIG_VALUES::PACKET_SIZE = s;
	}

	void SET_BUFFER_SIZE(const int s)
	{
		CONFIG_VALUES::BUFFER_SIZE = s;
	}

	void SET_UNDO_LEVELS(const int level) {
		CONFIG_VALUES::UNDO_LEVELS = level;
	}

	void SET_MAX_AREA(const int max) {
		CONFIG_VALUES::MAX_AREA = max;
	}

	void SET_MIN_AREA(const int min) {
		CONFIG_VALUES::MIN_AREA = min;
	}

	void SET_SENDING_HOST(const std::string & host) {
		CONFIG_VALUES::SENDING_HOST = host;
	}

	void SET_SENDING_USR(const std::string & usr) {
		CONFIG_VALUES::SENDING_USR = usr;
	}

	void SET_SENDING_PWD(const std::string & pwd) {
		CONFIG_VALUES::SENDING_PWD = pwd;
	}

	void SET_RENDER_HOST(const std::string & host) {
		CONFIG_VALUES::RENDER_HOST = host;
	}

} /* namespace CONFIG */